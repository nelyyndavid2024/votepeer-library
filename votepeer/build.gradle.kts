import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
    kotlin("plugin.serialization")
    `maven-publish`
    jacoco
}

android {
    signingConfigs {
        create("debugVotePeerLib") {
        }
    }
    compileSdk = 31
    buildToolsVersion = "31.0.0"

    defaultConfig {

        minSdk = 26
        targetSdk = 31

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        getByName("debug") {
            isJniDebuggable = true
            isRenderscriptDebuggable = true
            isMinifyEnabled = false
        }
        create("main")
        create("debugRelease") {
            isJniDebuggable = true
            isRenderscriptDebuggable = true
            isMinifyEnabled = false
            isTestCoverageEnabled = false
        }
        create("debugVotePeerLib")
        getByName("release") {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    sourceSets {
        all {
            java.srcDir("src/$name/kotlin")
        }
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
        dataBinding = true
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = Version.compose
    }

    testOptions.unitTests.isIncludeAndroidResources = true
    testOptions.animationsDisabled = false
    testOptions.unitTests.isReturnDefaultValues = true

    useLibrary("android.test.base")
    useLibrary("android.test.mock")

    packagingOptions {
        exclude("META-INF/DEPENDENCIES")
        exclude("META-INF/LICENSE")
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/license.txt")
        exclude("META-INF/LICENSE.md")
        exclude("META-INF/LICENSE-notice.md")
        exclude("META-INF/NOTICE")
        exclude("META-INF/NOTICE.txt")
        exclude("META-INF/notice.txt")
        exclude("META-INF/ASL2.0")
        exclude("META-INF/AL2.0")
        exclude("META-INF/LGPL2.1")
        exclude("META-INF/*.kotlin_module")
        jniLibs {
            useLegacyPackaging = true
        }
    }

    lint {
        this.isAbortOnError = false
    }
}

abstract class DependencyVersions(
    val lifecycle: String,
    val navigation: String,
    val espresso: String,
    val androidxTest: String,
    val jupiter: String,
    val coroutinesAndroid: String,
    val ktor: String,
)

object Version : DependencyVersions(
    "2.3.1", // lifecycle
    "2.3.5", // navigation
    "3.3.0", // espresso -- 3.4.0 crashed Android UI-tests.
    // reference: https://stackoverflow.com/questions/67358179/android-espresso-test-error-no-static-method-loadsingleserviceornull
    "1.4.0",
    "5.7.2", // jupiter
    "1.4.3", // coroutinesAndroid
    "1.6.0", // ktor
) {
    const val libbitcoincash = "3.0.95"
    const val compose = "1.1.0-rc01"
    const val room = "2.4.0-rc01"
}

dependencies {
    implementation("info.bitcoinunlimited", "libbitcoincash", Version.libbitcoincash)
    // debugImplementation("info.bitcoinunlimited", "libbitcoincash", Version.libbitcoincash)
    // androidTestImplementation("info.bitcoinunlimited", "libbitcoincash", Version.libbitcoincash)
    // debugImplementation(project(":libbitcoincash"))
    // debugLocalImplementation("info.bitcoinunlimited", "libbitcoincash", Version.libbitcoincash)
    androidTestImplementation(project(":libbitcoincash"))

    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.core", "core-ktx", "1.7.0")
    implementation("androidx.appcompat", "appcompat", "1.4.0")

    // Kotlin
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.1")
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-android", Version.coroutinesAndroid)
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-play-services", "1.3.3")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.5.31")

    // Android
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.constraintlayout", "constraintlayout", "2.0.4")
    implementation("androidx.lifecycle", "lifecycle-runtime-ktx", Version.lifecycle)
    implementation("androidx.lifecycle", "lifecycle-viewmodel-ktx", Version.lifecycle)
    implementation("androidx.navigation", "navigation-fragment-ktx", Version.navigation)
    implementation("androidx.navigation", "navigation-ui-ktx", Version.navigation)
    implementation("androidx.swiperefreshlayout", "swiperefreshlayout", "1.0.0")
    kapt("com.android.databinding", "compiler", "3.1.4")

    // Compose
    implementation("androidx.activity:activity-compose:1.4.0")
    implementation("androidx.compose.ui:ui:${Version.compose}")
    implementation("androidx.compose.ui:ui-tooling:${Version.compose}")
    implementation("androidx.compose.material:material:${Version.compose}")
    implementation("androidx.compose.animation:animation:${Version.compose}")
    implementation("com.google.android.material:compose-theme-adapter:1.1.2")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.4.0")

    // UI Tests
    androidTestImplementation("androidx.compose.ui:ui-test:${Version.compose}")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:${Version.compose}")
    debugImplementation("androidx.compose.ui:ui-test-manifest:${Version.compose}")

    // Accompanist
    implementation("com.google.accompanist:accompanist-pager:0.20.3") // Pager
    implementation("com.google.accompanist:accompanist-pager-indicators:0.20.3")

    // Firebase
    implementation(platform("com.google.firebase:firebase-bom:26.0.0"))
    implementation("com.google.firebase", "firebase-auth-ktx")
    implementation("com.google.firebase", "firebase-core")
    implementation("com.google.firebase", "firebase-firestore-ktx")
    implementation("com.google.firebase", "firebase-functions-ktx")
    implementation("com.google.firebase", "firebase-messaging")

    // Ktor
    implementation("io.ktor:ktor-client-okhttp:${Version.ktor}")
    implementation("io.ktor:ktor-client-json:${Version.ktor}")
    implementation("io.ktor:ktor-client-serialization-jvm:${Version.ktor}")
    implementation("io.ktor:ktor-client-logging-jvm:${Version.ktor}")

    // QR Scanner + QR generator
    implementation("com.journeyapps", "zxing-android-embedded", "4.1.0")
    implementation("com.google.zxing", "core", "3.4.0")

    implementation("com.ncorti:slidetoact:0.9.0")

    // Test helpers
    debugImplementation("androidx.fragment:fragment-testing:1.4.0")
    testImplementation("androidx.room", "room-testing", Version.room)

    // Testing libraries
    implementation("androidx.room", "room-runtime", Version.room)
    implementation("androidx.room", "room-ktx", Version.room)
    kapt("androidx.room", "room-compiler", Version.room)
    kaptAndroidTest("com.android.databinding", "compiler", "3.1.4")

    testImplementation("androidx.test", "core-ktx", Version.androidxTest)
    testImplementation("androidx.test.ext", "truth", Version.androidxTest)
    testImplementation("androidx.arch.core:core-testing:2.1.0")
    testImplementation("io.mockk:mockk:1.11.0")
    testImplementation("androidx.room", "room-testing", Version.room)

    // AndroidJUnitRunner and JUnit Rules
    androidTestImplementation("androidx.test", "runner", Version.androidxTest)
    androidTestImplementation("androidx.test", "rules", Version.androidxTest)
    androidTestImplementation("org.jetbrains.kotlinx", "kotlinx-coroutines-test", Version.coroutinesAndroid)
    androidTestImplementation("androidx.navigation", "navigation-testing", Version.navigation)
    androidTestImplementation("androidx.test.espresso", "espresso-core", Version.espresso)
    androidTestImplementation("androidx.test.espresso", "espresso-contrib", Version.espresso)
    androidTestImplementation("androidx.test.espresso", "espresso-intents", Version.espresso)
    androidTestImplementation("org.junit.jupiter", "junit-jupiter-api", Version.jupiter)
    androidTestImplementation("org.junit.jupiter", "junit-jupiter-params", Version.jupiter)
    androidTestImplementation("info.bitcoinunlimited", "libbitcoincash", Version.libbitcoincash)
    androidTestImplementation("androidx.test.ext:truth:1.4.0")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("io.mockk:mockk-android:1.11.0")
    androidTestImplementation("androidx.arch.core:core-testing:2.1.0")

    // (Required) Writing and executing Unit Tests on the JUnit Platform
    testImplementation("org.junit.jupiter", "junit-jupiter-api", Version.jupiter)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", Version.jupiter)

    // (Optional) If you need "Parameterized Tests"
    testImplementation("org.junit.jupiter", "junit-jupiter-params", Version.jupiter)
}

fun getVersionCode(): String {
    val buildNumber = System.getenv("BUILD_NUMBER") ?: "999"
    println("buildNumber: $buildNumber")
    return "4.3.$buildNumber"
}

group = "info.bitcoinunlimited"
version = getVersionCode()

val releaseAarPath = "$buildDir/outputs/aar/votepeer-release.aar"

publishing {
    publications {
        create<MavenPublication>("votepeer") {
            artifact(releaseAarPath)
        }
    }

    repositories {
        maven {
            url = uri("$buildDir/repos")
        }
    }
}

tasks.map {
    if (it.name.startsWith("publish")) {
        // Make sure library has been built when publishing
        it.doFirst {
            if (!file(releaseAarPath).exists()) {
                println("Project has not been built. Please run task 'assembleRelease' first.")
                assert(false)
            }
        }
    }
}

fun Project.getKtlintConfiguration(): Configuration {
    return configurations.findByName("ktlint") ?: configurations.create("ktlint") {
        val dependency = project.dependencies.create("com.pinterest:ktlint:0.42.1")
        dependencies.add(dependency)
    }
}

fun lintedFileList(): List<String> {
    return listOf(
        "build.gradle.kts",
        "src/**/*.kt"
    )
}

tasks.register("ktlint", JavaExec::class.java) {
    description = "Check Kotlin code style."
    group = "Verification"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android") + lintedFileList()
}
tasks.register("ktlintFormat", JavaExec::class.java) {
    description = "Fix Kotlin code style deviations."
    group = "formatting"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("-F", "--android") + lintedFileList()
    if (javaVersion.isJava9Compatible) {
        // Fix for InaccessibleObjectException after upgrading from Java 1.8
        // https://github.com/gradle/gradle/issues/1095#issuecomment-270476256
        jvmArgs = mutableListOf("--add-opens", "java.base/java.lang=ALL-UNNAMED")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()

    extensions.configure(JacocoTaskExtension::class) {
        this.setDestinationFile(file("$buildDir/jacoco/jacoco.exec"))
    }
}

tasks.withType<JacocoReport> {
    reports {
        html.required.set(true)
        html.isEnabled = true
        // html.destination = file("$buildDir/reports/coverage")

        xml.required.set(true)
        xml.isEnabled = true
        xml.destination = file("${project.buildDir}/jacoco/jacoco.xml")
    }
}

tasks.register<JacocoCoverageVerification>("jacocoTestCoverageVerification") {
    violationRules {
        rule {
            limit {
                minimum = "0.9".toBigDecimal()
            }
        }
    }
}

val testCoverage by tasks.registering {
    group = "verification"
    description = "Runs the unit tests with coverage."

    dependsOn(":test", ":jacocoTestReport", ":jacocoTestCoverageVerification")
    val jacocoTestReport = tasks.findByName("jacocoTestReport")
    jacocoTestReport?.mustRunAfter(tasks.findByName("test"))
    tasks.findByName("jacocoTestCoverageVerification")?.mustRunAfter(jacocoTestReport)
}

jacoco {
    toolVersion = "0.8.7"
}
