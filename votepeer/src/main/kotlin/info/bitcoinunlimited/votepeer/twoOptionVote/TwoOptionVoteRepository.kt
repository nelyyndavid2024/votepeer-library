package info.bitcoinunlimited.votepeer.twoOptionVote

import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVoteContract
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.VoteRepository
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.election.TwoOptionVoteElection
import info.bitcoinunlimited.votepeer.tally.Tally
import info.bitcoinunlimited.votepeer.utils.TAG_TWO_OPTION_VOTE_REPOSITORY
import info.bitcoinunlimited.votepeer.vote.Vote
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteTransaction
import kotlinx.coroutines.*

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@InternalCoroutinesApi
class TwoOptionVoteRepository(
    override val chain: ChainSelector,
    override val election: TwoOptionVoteElection,
    electionService: ElectionService,
    electrum: ElectrumAPI,
    identityRepository: IdentityRepository,
) : VoteRepository(identityRepository, electionService, electrum) {
    private val optionAHash: ByteArray = election.getOptionHash("0")
    private val optionBHash: ByteArray = election.getOptionHash("1")

    @Suppress("MemberVisibilityCanBePrivate")
    internal fun getContract(voteElection: TwoOptionVoteElection): TwoOptionVoteContract {
        val contractElectionId = voteElection.getContractElectionID()
        val publicKeyHash = identityRepository.currentUser.pkh() ?: throw Exception("Cannot get pkh in TwoOptionVoteRepository!")
        return TwoOptionVoteContract(
            contractElectionId,
            optionAHash,
            optionBHash,
            publicKeyHash
        )
    }

    @Deprecated("Deprecate for Two Option Vote")
    override suspend fun getVoteStringHuman(): String {
        return "(Deprecated) Click to view in browser"
    }

    override suspend fun getVoteOption(): VoteOption {
        val voteTx = fetchOurVoteTransaction() ?: return VoteOption(VoteOption.NOT_VOTED_MAGIC_INDEX)
        return electionService.getVote(election, voteTx).option
    }

    override suspend fun getVoteTxId(): String {
        val voteTx = fetchOurVoteTransaction() ?: throw Exception("Cannot fetch voteTx")
        return voteTx.voteTxId
    }

    override suspend fun vote(option: VoteOption): Vote {
        val votingUser = identityRepository.currentUser
        val contractElectionId = election.getContractElectionID()
        val twoOptionVote = TwoOptionVoteContract(
            contractElectionId,
            optionAHash,
            optionBHash,
            votingUser.pkh()!!
        )

        val coin = electrum.getOrCreateUtxo(
            twoOptionVote.address(chain),
            votingUser.address!!,
            TwoOptionVoteContract.MIN_CONTRACT_INPUT,
            votingUser.secret!!,
            votingUser.secret
        )
        val castInput = BCHinput(chain, coin, BCHscript(chain))
        val voteTx = twoOptionVote.castVote(
            chain,
            castInput,
            votingUser,
            election.getOptionHash(option.selected)
        )
        electrum.broadcast(voteTx)
        return Vote(option, VoteTransaction.TwoOption(voteTx))
    }

    override suspend fun getTally(publicKeyHash: ByteArray): Tally {
        val contracts = election.participants.map { participantPkh ->
            val contractElectionId = election.getContractElectionID()
            TwoOptionVoteContract(
                contractElectionId,
                optionAHash,
                optionBHash,
                participantPkh
            )
        }

        val option: MutableMap<String, MutableList<Pair<String?, String?>>> = mutableMapOf()

        // Initiate tally
        for (o in election.options) {
            option[o] = mutableListOf()
        }
        option[VoteOption.NOT_VOTED_MAGIC_INDEX] = mutableListOf()

        for (c in contracts) {
            val voterAddress = PayAddress(chain, PayAddressType.P2PKH, c.voterPKH).toString()
            val (_, voteTx) = fetchVoteTransaction(
                c.address(chain), election.endHeight
            ) ?: Pair(null, null)

            if (voteTx != null) {
                val selectedOption = electionService.getVote(election, voteTx).option.selected
                option[selectedOption]!!.add(Pair(voteTx.voteTxHash.toHex(), voterAddress))
            } else {
                // User has not vote yet.
                option[VoteOption.NOT_VOTED_MAGIC_INDEX]!!.add(Pair(null, voterAddress))
            }
        }
        return Tally(election.id, option)
    }

    /**
     * If the transaction is confirmed it can be cached.
     * (This assumes low risk of reorg with double spends).
     */
    private var cachedVoteTransaction: VoteTransaction? = null

    /**
     * Fetch the vote transaction for THIS user.
     */
    private suspend fun fetchOurVoteTransaction(): VoteTransaction? {
        cachedVoteTransaction?.let {
            return it
        }
        val (height, tx) = fetchVoteTransaction(
            getContract(election).address(chain),
            election.endHeight
        ) ?: return null

        if (!ElectrumAPI.isInMempool(height)) {
            cachedVoteTransaction = tx
        }
        return tx
    }

    /**
     * Fetches a vote transaction from the blockchain if there is one, otherwise null.
     * Raises error if there vote is spoilt.
     */
    suspend fun fetchVoteTransaction(contractAddress: PayAddress, endHeight: Long): Pair<Long, VoteTransaction>? {

        val txs = electrum.getSpendingTransactions(contractAddress, null, endHeight).sortedByDescending {
            it.first
        }
        var bestMatch: Pair<Long, BCHtransaction>? = null
        Log.w(TAG_TWO_OPTION_VOTE_REPOSITORY, txs.toString())
        for (tx in txs) {
            val height = tx.first
            if (bestMatch == null) {
                bestMatch = tx
                continue
            }

            // Vote is not spoilt.
            return Pair(bestMatch.first, VoteTransaction.TwoOption(bestMatch.second))
        }
        if (bestMatch?.second === null)
            return null
        else
            return Pair(bestMatch.first, VoteTransaction.TwoOption(bestMatch.second))
    }
}