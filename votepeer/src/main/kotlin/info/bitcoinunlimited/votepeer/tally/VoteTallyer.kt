package info.bitcoinunlimited.votepeer.tally

import kotlinx.coroutines.InternalCoroutinesApi

interface VoteTallyer {
    @InternalCoroutinesApi
    suspend fun getTally(pkh: ByteArray?): Tally
}
