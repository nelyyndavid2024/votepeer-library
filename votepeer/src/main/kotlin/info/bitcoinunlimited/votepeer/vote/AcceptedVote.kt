package info.bitcoinunlimited.votepeer.vote

import bitcoinunlimited.libbitcoincash.BCHtransaction

/**
 * Intermediate data used during vote tallying. Used in `RingSignatureVoteTallyer`.
 */
data class AcceptedVote(
    val vote: ByteArray,
    val signature: ByteArray,
    val voteOption: VoteOption,
    val tx: BCHtransaction
) {
    var exposedOwner: ByteArray? = null
}