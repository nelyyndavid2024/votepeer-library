package info.bitcoinunlimited.votepeer.vote

/*
 When data class Vote is available it is safe to set RingSignatureVote in ROOM local storage because VoteTransaction is non-nullable.
 Therefore the transaction should have been broadcast to BCH.
 */

data class Vote(
    val option: VoteOption,
    val transaction: VoteTransaction
)
