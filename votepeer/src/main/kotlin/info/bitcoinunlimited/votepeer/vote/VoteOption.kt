package info.bitcoinunlimited.votepeer.vote

import info.bitcoinunlimited.votepeer.databinding.FragmentElectionDetailBinding
import java.io.Serializable
import java.util.*

data class VoteOption(
    val selected: String,
    val selectedTime: Calendar = Calendar.getInstance(),
    val selectedTimeMilliseconds: Long = selectedTime.timeInMillis
) : Serializable {

    companion object {
        /**
         * Magic index representing that vote has not been cast
         */
        const val NOT_VOTED_MAGIC_INDEX = "0xc0ffee"

        fun getSelectedVoteOption(binding: FragmentElectionDetailBinding): VoteOption {
            val voteOptionAId = 0
            val voteOptionBId = 1

            /*

            val option = when (binding.electionDetailRadioGroup.checkedRadioButtonId) {
                binding.electionOptionAButton.id -> voteOptionAId
                binding.electionOptionBButton.id -> voteOptionBId
                binding.electionAbstain.id -> VoteOption.ABSTAIN_MAGIC_INDEX
                else -> {
                    val errorMessage = "Cannot find radiobutton id that was selected when voting in ElectionDetailFragment"
                    Log.e(TAG_ELECTION_DETAIL, errorMessage)
                    throw Exception(errorMessage)
                }
            }
            return VoteOption(option)
             */
            TODO("Implenent getSelectedVoteOption")
        }
    }
}
