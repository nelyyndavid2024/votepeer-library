package info.bitcoinunlimited.votepeer.election.detail

import android.net.Uri
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.VoteRepository
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionLifecycle
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteNotFoundInRoomException
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDao
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteRoom
import info.bitcoinunlimited.votepeer.utils.Constants.EXPLORER_URL
import info.bitcoinunlimited.votepeer.utils.TAG_ELECTION_DETAIL
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteOption.Companion.NOT_VOTED_MAGIC_INDEX
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.*

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailViewModel(
    private val dispatcher: CoroutineDispatcher,
    val election: Election,
    private val electrumAPI: ElectrumAPI,
    private var latestBlockHeightRaw: Long,
    private val ringSignatureVoteDao: RingSignatureVoteDao, // TODO: Replace with repository
    private val voteRepository: VoteRepository,
) : ViewModel() {
    private val _loading = MutableStateFlow(true)
    private val _error = MutableStateFlow<Throwable?>(null)
    private val _voted = MutableStateFlow<Boolean?>(null)
    private val _lifecycle = MutableStateFlow<ElectionLifecycle?>(null)
    private val _selectedOption = MutableStateFlow<String?>(null)
    private val _voteUrl = MutableStateFlow<Uri?>(null)
    private val _latestBlockHeight = MutableStateFlow<Long>(latestBlockHeightRaw)
    val loading = _loading.asStateFlow()
    val error = _error.asStateFlow()
    val voted = _voted.asStateFlow()
    val lifecycle = _lifecycle.asStateFlow()
    val selectedOption = _selectedOption.asStateFlow()
    val voteUrl = _voteUrl.asStateFlow()
    val latestBlockHeight = _latestBlockHeight.asStateFlow()

    private val _state = MutableStateFlow<ElectionDetailState?>(null)
    private val _stateError = MutableStateFlow<ElectionDetailViewStateError?>(null)
    val state = _state.asStateFlow()

    internal val handler = CoroutineExceptionHandler { _, throwable ->
        Log.e(TAG_ELECTION_DETAIL, throwable.message ?: "Something went wrong in ElectionDetailViewModel")
        _error.value = throwable
        _loading.value = false
    }
    private var radioGroupOptionSelected = false
    private var hasVoted: Boolean? = null

    init {
        initialize()
    }

    internal fun initialize() = viewModelScope.launch(dispatcher + handler) {
        _loading.value = true
        val voteOption = fetchVoteOption()
        _selectedOption.value = voteOption.selected
        _latestBlockHeight.value = electrumAPI.getLatestBlockHeight() ?: latestBlockHeight.value
        _lifecycle.value = election.getElectionLifecycleState(latestBlockHeight.value)
        if (voteOption.selected == NOT_VOTED_MAGIC_INDEX) {
            _voted.value = false
            _voteUrl.value = null
        } else {
            val voteTxId = voteRepository.getVoteTxId()
            _voted.value = true
            _voteUrl.value = Uri.parse("$EXPLORER_URL$voteTxId")
        }
        _loading.value = false
    }

    // TODO: Deprecate?
    internal fun getElectionDetailState(election: Election, selected: Boolean, voteOption: VoteOption) {
        val lifecycleViewState = election.getElectionLifecycleState(latestBlockHeight.value)
        _state.value = if (
            election is RingSignatureVoteElection &&
            _voted.value == false &&
            voteRepository is RingSignatureVoteRepository
        ) {
            if (voteOption.selected != NOT_VOTED_MAGIC_INDEX) {
                val canBeFundedExternally = voteRepository.canBeFundedExternally(voteOption)
                if (canBeFundedExternally) {
                    val fundingRequired = voteRepository.vote.calculateFundingAmountRequired(numberOfInputs = 1)
                    val fundingAddress = voteRepository.getFundingAddress(voteOption).toString()
                    val fundingAddressQrCode = voteRepository.generateFundingQrCode(fundingAddress, qrHeight = 512, qrWidth = 512)
                    setState(ElectionDetailState.FundableRingSignatureVote(fundingAddressQrCode, fundingRequired))
                }
                ElectionDetailState.VoteElectionViewState(hasVoted, selected, lifecycleViewState)
            } else {
                ElectionDetailState.VoteElectionViewState(hasVoted, selected, lifecycleViewState)
            }
        } else {
            ElectionDetailState.VoteElectionViewState(hasVoted, selected, lifecycleViewState)
        }
    }

    fun updateSelectedOption(option: String) {
        _selectedOption.value = option
    }

    internal fun setHasVoted(voted: Boolean, voteTxId: String?) {
        _voted.value = voted
        voteTxId?.let {
            _voteUrl.value = Uri.parse("$EXPLORER_URL$it")
        }
        val lifecycleViewState = election.getElectionLifecycleState(latestBlockHeight.value)
        _lifecycle.value = lifecycleViewState
        val voteStatusViewState = ElectionDetailState.VoteElectionViewState(voted, radioGroupOptionSelected, lifecycleViewState)
        setState(voteStatusViewState)
    }

    private val setStateHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e(TAG_ELECTION_DETAIL, throwable.message ?: "Something went wrong in setElectionDetailState")
        _loading.value = false
        _stateError.value = ElectionDetailViewStateError(Exception(throwable))
    }

    private fun setState(state: ElectionDetailState) = viewModelScope.launch(Dispatchers.Main + setStateHandler) {
        _state.value = state
    }

    internal fun vote(option: String) = viewModelScope.launch(dispatcher + handler) {
        _loading.value = true
        _latestBlockHeight.value = electrumAPI.getLatestBlockHeight() ?: latestBlockHeight.value
        val vote = voteRepository.vote(VoteOption(option))
        _voted.value = true
        _voteUrl.value = Uri.parse("$EXPLORER_URL${vote.transaction.voteTxId}")
        _selectedOption.value = vote.option.selected
        if (election is RingSignatureVoteElection) {
            val ringSignatureVote = RingSignatureVoteRoom(election, vote)
            ringSignatureVoteDao.insert(ringSignatureVote)
        }
        _loading.value = false
    }

    internal suspend fun fetchVoteOption(): VoteOption {
        return try {
            voteRepository.getVoteOption()
        } catch (throwable: Throwable) {
            return when (throwable) {
                is RingSignatureVoteNotFoundInRoomException -> {
                    VoteOption(NOT_VOTED_MAGIC_INDEX)
                }
                else -> {
                    throw Exception(throwable)
                }
            }
        }
    }

    @DelicateCoroutinesApi
    class ElectionDetailViewModelFactory(
        private val dispatcher: CoroutineDispatcher,
        private val election: Election,
        private val electrumRepository: ElectrumAPI,
        private val currentBlockHeight: Long,
        private val ringSignatureVoteDao: RingSignatureVoteDao, // TODO: Move to repository
        private val voteRepository: VoteRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ElectionDetailViewModel::class.java))
                return ElectionDetailViewModel(
                    dispatcher,
                    election,
                    electrumRepository,
                    currentBlockHeight,
                    ringSignatureVoteDao,
                    voteRepository
                ) as T
            else
                throw IllegalArgumentException("ViewModel class must be a subclass of ElectionDetailViewModel!")
        }
    }
}
