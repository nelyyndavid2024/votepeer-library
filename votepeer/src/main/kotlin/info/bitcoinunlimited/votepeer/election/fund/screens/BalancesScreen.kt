package info.bitcoinunlimited.votepeer.election.fund

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import info.bitcoinunlimited.votepeer.R

@Composable
fun Balances(contractState: ContractState) {
    val bitmap = painterResource(id = R.drawable.bitcoin_cash_circle)
    val costText = contractState.cost.toBigDecimal().toPlainString() + " BCH"
    Column {
        Text(
            text = "Required balance",
            color = Color.DarkGray,
            textAlign = TextAlign.Left,
            fontSize = 20.sp
        )

        Row(modifier = Modifier.align(Alignment.Start).padding(top = 12.dp)) {
            Image(
                bitmap,
                contentDescription = "Smart contract address QR-code",
                modifier = Modifier.size(25.dp)
            )
            Text(
                text = costText,
                color = Color.DarkGray,
                textAlign = TextAlign.Left,
                fontSize = 20.sp,
                modifier = Modifier.padding(start = 8.dp)
            )
        }

        Text(
            text = "Balance",
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start).padding(top = 12.dp),
            textAlign = TextAlign.Left,
            fontSize = 20.sp
        )
        var balanceText = "."
        balanceText = when (contractState) {
            is ContractState.LoadingBalance -> {
                contractState.message
            }
            is ContractState.Contract -> {
                contractState.balance.toBigDecimal().toPlainString() + " BCH"
            }
        }

        Row(modifier = Modifier.align(Alignment.Start).padding(top = 12.dp)) {
            Image(
                bitmap,
                contentDescription = "Smart contract address QR-code",
                modifier = Modifier.size(25.dp)
            )
            Text(
                text = balanceText,
                color = Color.DarkGray,
                modifier = Modifier.padding(start = 8.dp),
                textAlign = TextAlign.Left,
                fontSize = 20.sp
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BalancesPreview() {
    Balances(ContractState.Contract(0.00069, 0.1337, "voteOptionHuman"))
}