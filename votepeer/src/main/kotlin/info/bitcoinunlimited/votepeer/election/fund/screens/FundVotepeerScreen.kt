package info.bitcoinunlimited.votepeer.election.fund

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import info.bitcoinunlimited.votepeer.R

@Composable
fun FundVotePeerScreen(electionTitle: String, optionHuman: String) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.colorWhite))
            .padding(16.dp)
    ) {
        Text(
            text = electionTitle,
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
        Text(
            text = optionHuman,
            modifier = Modifier.align(Alignment.Start),
            textAlign = TextAlign.Left,
            fontSize = 16.sp
        )

        Text(
            text = "Warning",
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start).padding(top = 25.dp),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
        Text(
            text = "Your vote will not be anonymous when funding the contract with VotePeer. It is recommended to use an external wallet.",
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start),
            textAlign = TextAlign.Left,
            fontSize = 16.sp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun VotePeerScreenPreview() {
    FundVotePeerScreen(
        "Election title!",
        "Voting for option: The rent is too damn high!",
    )
}