package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.vote.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVote
import info.bitcoinunlimited.votepeer.vote.Vote
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteTransaction
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
@ExperimentalUnsignedTypes
/*
    Combines data sources from VotePeer REST-API and Electron-Cash RPC

    // TODO: Rename to VoteService?
 */
class ElectionService(private val chain: ChainSelector) {

    fun getVoteOption(election: Election, voteOptionHash: ByteArray): VoteOption {
        return ElectionService.getVoteOption(election, voteOptionHash)
    }

    fun getVote(election: Election, tx: VoteTransaction): Vote {
        val voteOptionHash = parseVote(tx, election)
        val voteOption = VoteOption(getVoteOption(election, voteOptionHash).selected)
        val voteTransaction = tx
        return Vote(voteOption, voteTransaction)
    }

    fun parseVote(tx: VoteTransaction, election: Election): ByteArray {
        return when (election) {
            is TwoOptionVoteElection -> TwoOptionVote.parseVote(tx.voteTx)
            is RingSignatureVoteElection -> RingSignatureVoteBase.parseVote(chain, tx.voteTx).first
            else -> throw Exception("Type not supported")
        }
    }

    fun parseVoteSentence(election: Election, voteOptionHash: ByteArray): String {
        election.options.forEach { i ->
            if (voteOptionHash.contentEquals(election.getOptionHash(i))) {
                return "✅ You voted $i"
            }
        }

        // Special case for TwoOptionVote vote contract
        if (voteOptionHash.contentEquals(TwoOptionVoteContract.BLANK_VOTE)) {
            return "✅ You voted abstain"
        }
        return "⚠️ Vote now!"
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: ElectionService? = null

        fun getInstance(
            chain: ChainSelector
        ) = instance ?: synchronized(this) {
            instance
                ?: ElectionService(chain)
                    .also { instance = it }
        }

        fun getVoteOption(election: Election, voteOptionHash: ByteArray): VoteOption {
            election.options.forEach { i ->
                if (voteOptionHash.contentEquals(election.getOptionHash(i))) {
                    return VoteOption(i)
                }
            }
            throw Exception("Cannot get vote option")
        }
    }
}
