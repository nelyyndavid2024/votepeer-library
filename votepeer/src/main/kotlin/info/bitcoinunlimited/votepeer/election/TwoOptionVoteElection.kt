package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.vote.TwoOptionVote
import info.bitcoinunlimited.votepeer.election.network.ElectionRaw
import java.io.Serializable

@Deprecated("Use multiOptionElection")
data class TwoOptionVoteElection(
    val electionRaw: ElectionRaw,
    override val participants: Array<ByteArray>
) : Serializable, Election(electionRaw) {
    override fun getContractElectionID(): ByteArray {
        return TwoOptionVote.calculate_proposal_id(
            electionRaw.salt.toByteArray(),
            electionRaw.description,
            electionRaw.options[0],
            electionRaw.options[1],
            electionRaw.endHeight.toInt(),
            participants
        )
    }
}