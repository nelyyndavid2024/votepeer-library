package info.bitcoinunlimited.votepeer.election.fund

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import info.bitcoinunlimited.votepeer.R

@Composable
fun FundExternalScreen(
    electionTitle: String,
    optionHuman: String,
    contractState: ContractState,
    contractAddress: String,
    qrCode: Painter,
    copyAddress: () -> Unit,
    shareAddress: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.colorWhite))
            .padding(16.dp)
    ) {
        Text(
            text = electionTitle,
            fontWeight = FontWeight.Bold,
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start),
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
        )
        Text(
            text = "Selected option: $optionHuman",
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start),
            textAlign = TextAlign.Left,
            fontSize = 14.sp
        )
        Text(
            fontWeight = FontWeight.Bold,
            text = "Smart contract address",
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start).padding(top = 24.dp),
            textAlign = TextAlign.Left,
            fontSize = 25.sp
        )
        Text(
            text = "Fund to activate voting",
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start).padding(top = 6.dp),
            textAlign = TextAlign.Left,
            fontSize = 14.sp
        )
        Text(
            text = contractAddress,
            color = Color.DarkGray,
            modifier = Modifier.align(Alignment.Start).padding(top = 6.dp),
            textAlign = TextAlign.Left,
            fontSize = 14.sp
        )
        Row(modifier = Modifier.align(Alignment.Start).padding(top = 12.dp)) {
            Image(
                qrCode,
                contentDescription = "Smart contract address QR-code",
                modifier = Modifier
                    .size(180.dp)
            )
            Balances(contractState)
        }
        Button(
            onClick = {
                copyAddress()
            },
            modifier = Modifier.padding(top = 12.dp)
        ) {
            Icon(
                painter = painterResource(R.drawable.content_copy),
                contentDescription = null,
            )
            Text(
                text = "Copy",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }
        Button(onClick = {
            shareAddress()
        }) {
            Icon(
                imageVector = Icons.Default.Share,
                contentDescription = null,
            )
            Text(
                text = "Share",
                textAlign = TextAlign.Center,
                modifier = Modifier.align(Alignment.Bottom).fillMaxWidth(),
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun FundExternalScreenPreview() {
    val bitmap = painterResource(id = R.drawable.ic_external_black)
    FundExternalScreen(
        "Election title",
        "Voting for option: The rent is too damn high!",
        ContractState.Contract(0.00069, 0.1337, "Voting for option: The rent is too damn high!"),
        "qpv5y82t8z7n6w80fpm64afah7ntptxue59h5cdsn2",
        bitmap,
        { }
    ) {
    }
}