package info.bitcoinunlimited.votepeer.election.network

import kotlinx.serialization.Serializable

@Serializable
data class ListParticipatingElectionsResult(
    val cursor: String?,
    val elections: List<ElectionRaw>
)