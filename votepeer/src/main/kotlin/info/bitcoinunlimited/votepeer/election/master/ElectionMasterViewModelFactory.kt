package info.bitcoinunlimited.votepeer.election.master

import android.app.Application
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.ChainSelector
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.election.network.ElectionProviderRest
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val electionService: ElectionService,
    private val authRepository: AuthRepository,
    private val electrumApi: ElectrumAPI,
    private val restProviderElection: ElectionProviderRest,
    private val chain: ChainSelector,
    private val application: Application,
) : AbstractSavedStateViewModelFactory(owner, null) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
        ElectionMasterViewModel(
            electionService,
            authRepository,
            authRepository.identityRepository,
            electrumApi,
            restProviderElection,
            chain,
            application
        ) as T
}
