package info.bitcoinunlimited.votepeer.election.master

import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.election.Election

sealed class ElectionMasterViewState {
    data class LoadingSpinner(
        val loading: Boolean = false
    ) : ElectionMasterViewState()

    data class ErrorElectionMaster(
        val exception: Exception
    ) : ElectionMasterViewState()

    data class Auth(
        val authState: AuthState
    ) : ElectionMasterViewState()
}

data class ElectionsViewState(
    val elections: List<Election>
)
