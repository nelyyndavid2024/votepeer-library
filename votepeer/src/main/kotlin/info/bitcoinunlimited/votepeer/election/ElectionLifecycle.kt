package info.bitcoinunlimited.votepeer.election

enum class ElectionLifecycle {
    NOT_STARTED, ONGOING, ENDED
}