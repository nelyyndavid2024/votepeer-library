package info.bitcoinunlimited.votepeer.election.fund

sealed class ContractState {
    abstract val cost: Double
    abstract val voteOptionHuman: String
    data class LoadingBalance(
        val message: String = "Contract balance loading...",
        override val cost: Double,
        override val voteOptionHuman: String
    ) : ContractState()
    data class Contract(
        val balance: Double,
        override val cost: Double,
        override val voteOptionHuman: String
    ) : ContractState() {
        val isFunded = balance > cost
    }
}