package info.bitcoinunlimited.votepeer.election.detail

import android.graphics.Bitmap
import info.bitcoinunlimited.votepeer.election.ElectionLifecycle
import info.bitcoinunlimited.votepeer.vote.VoteOption

sealed class ElectionDetailState {
    data class Network(
        val fetching: Boolean
    ) : ElectionDetailState()

    data class VoteOptionViewState(
        val voteOption: VoteOption,
    ) : ElectionDetailState()

    data class FundableRingSignatureVote(
        val qrCode: Bitmap,
        val fundingRequired: Int,
    ) : ElectionDetailState()

    data class VoteElectionViewState(
        val voted: Boolean?,
        val selected: Boolean,
        val lifecycle: ElectionLifecycle,
    ) : ElectionDetailState() {
        val hasVotedAndElectionOngoing: Boolean?
        val hasNotVotedAndNotSelectedElectionOnGoing: Boolean?
        val hasNotVotedAndSelectedElectionOnGoing: Boolean?

        init {
            if (voted != null) {
                hasVotedAndElectionOngoing = voted && lifecycle == ElectionLifecycle.ONGOING
                hasNotVotedAndNotSelectedElectionOnGoing = !voted && lifecycle == ElectionLifecycle.ONGOING && !selected
                hasNotVotedAndSelectedElectionOnGoing = !voted && lifecycle == ElectionLifecycle.ONGOING && selected
            } else {
                hasVotedAndElectionOngoing = null
                hasNotVotedAndNotSelectedElectionOnGoing = null
                hasNotVotedAndSelectedElectionOnGoing = null
            }
        }
    }
}

data class ElectionDetailViewStateError(
    val exception: Exception
)