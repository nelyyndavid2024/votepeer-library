package info.bitcoinunlimited.votepeer.election.master

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class ElectionMasterViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val initElectionsViewState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val initElectrumApiState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val swipeToRefresh: MutableStateFlow<Event<SwipeToRefresh?>> = MutableStateFlow(Event(null))
) {
    object SwipeToRefresh
}
