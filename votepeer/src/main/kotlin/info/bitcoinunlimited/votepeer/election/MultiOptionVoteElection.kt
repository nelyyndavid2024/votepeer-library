package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.vote.MultiOptionVote
import info.bitcoinunlimited.votepeer.election.network.ElectionRaw
import java.io.Serializable

data class MultiOptionVoteElection(
    val electionRaw: ElectionRaw,
    override val participants: Array<ByteArray>,
) :
    Serializable, Election(electionRaw) {
    override fun getContractElectionID(): ByteArray {
        return getMultiOptionVote().electionId()
    }
    fun getMultiOptionVote(): MultiOptionVote {
        return MultiOptionVote(
            electionRaw.salt.toByteArray(),
            electionRaw.description,
            electionRaw.beginHeight.toInt(),
            electionRaw.endHeight.toInt(),
            electionRaw.options,
            participants
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MultiOptionVoteElection

        if (electionRaw != other.electionRaw) return false
        if (!participants.contentDeepEquals(other.participants)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = electionRaw.hashCode()
        result = 31 * result + participants.contentDeepHashCode()
        return result
    }
}
