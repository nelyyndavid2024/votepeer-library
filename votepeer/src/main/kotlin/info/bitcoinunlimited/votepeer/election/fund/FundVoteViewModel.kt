package info.bitcoinunlimited.votepeer.election.fund

import android.util.Log
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.BCHspendable
import bitcoinunlimited.libbitcoincash.PayAddress
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.ringSignatureVote.PayloadContractUtxo
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.utils.TAG_FUND_VOTE
import info.bitcoinunlimited.votepeer.vote.VoteOption
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class FundVoteViewModel(
    private val dispatcher: CoroutineDispatcher,
    private val voteRepository: RingSignatureVoteRepository,
    private val electrumAPI: ElectrumAPI,
    private val voteOption: VoteOption,
    private val identityRepository: IdentityRepository,
) : ViewModel() {
    internal val voteOptionHuman = voteOption.selected
    private val fundingRequired = voteRepository.vote.calculateFundingAmountRequired(numberOfInputs = 1).toDouble()
    private val _contractState = MutableStateFlow<ContractState>(
        ContractState.LoadingBalance(
            cost = fundingRequired,
            voteOptionHuman = voteOptionHuman
        )
    )
    private val _slideToVoteState = MutableStateFlow(false)
    private val _hasVotedState = MutableStateFlow(false)
    private val _exceptionState = MutableStateFlow<Exception?>(null)
    private val _satoshiRatio = 100000000.0
    private enum class CurrentTab {
        EXTERNAL_FUNDING,
        VOTEPEER_FUNDING
    }
    private var currentTab = CurrentTab.EXTERNAL_FUNDING
    val balanceState = _contractState.asStateFlow()
    private val fundingAddressWithPayload: Pair<PayAddress, ByteArray> = voteRepository.getFundingAddress(voteOption)
    var fundingAddress = fundingAddressWithPayload.first.toString()
    internal val handler = CoroutineExceptionHandler { _, throwable ->
        Log.e(TAG_FUND_VOTE, throwable.message ?: "Something went wrong in ElectionDetailViewModel")
        _exceptionState.value = Exception(throwable)
    }

    val qrCode = getQrCode(fundingAddress)

    internal fun getQrCode(address: String): Painter {
        val qrHeight = 512
        val qrWidth = 512
        val qrCode = voteRepository.generateFundingQrCode(address, qrHeight, qrWidth).asImageBitmap()
        return BitmapPainter(qrCode)
    }

    init {
        viewModelScope.launch(dispatcher + handler) {
            fetchContractBalance()
        }
    }

    fun unsubscribeAddress() {
        super.onCleared()
        GlobalScope.launch(Dispatchers.IO) {
            electrumAPI.unsubscribeAddress(fundingAddress)
        }
    }

    internal fun getFundingRequired(): Double {
        val fundingRequired = voteRepository.vote.calculateFundingAmountRequired(numberOfInputs = 1).toDouble()
        return (fundingRequired) / _satoshiRatio
    }

    internal fun subscribeAddress() {
        GlobalScope.launch(dispatcher + handler) {
            electrumAPI.subscribeToAddress(fundingAddress) {
                viewModelScope.launch(dispatcher + handler) {
                    fetchContractBalance()
                }
            }
        }
    }

    internal fun onTabChangeEvent(tab: Int) {
        when (tab) {
            0 -> {
                currentTab = CurrentTab.EXTERNAL_FUNDING
            }
            1 -> {
                setDisplayVotingSlider(true)
                currentTab = CurrentTab.VOTEPEER_FUNDING
            }
        }
    }

    internal suspend fun getBiggestCoin(): BCHspendable? {
        val coins = electrumAPI.listUnspent(fundingAddressWithPayload.first)

        // We only want one coin, so take the one with most value.
        return coins.maxByOrNull { c -> c.amount }
    }

    internal fun voteSliderEvent() {
        setDisplayVotingSlider(false)
        if (currentTab == CurrentTab.VOTEPEER_FUNDING) {
            voteUsingVotePeerFunds(voteOption)
        } else if (currentTab == CurrentTab.EXTERNAL_FUNDING) {
            viewModelScope.launch(Dispatchers.IO + handler) {
                val coins = electrumAPI.listUnspent(fundingAddressWithPayload.first)

                // We only want one coin, so take the one with most value.
                val utxo = getBiggestCoin() ?: return@launch // Not funded

                voteFundedExternally(voteOption, arrayOf(PayloadContractUtxo(fundingAddressWithPayload.second, utxo)))
            }
        }
    }

    internal suspend fun fetchContractBalance() {
        setContractState(
            ContractState.LoadingBalance(
                "Loading contract balance...",
                fundingRequired,
                voteOptionHuman
            )
        )
        val fundingRequired = getFundingRequired()

        val balance = getBiggestCoin()?.amount ?: 0
        val state = ContractState.Contract(
            if (balance == 0L) { 0.0 } else { balance.toDouble() / _satoshiRatio },
            fundingRequired, voteOptionHuman
        )
        setContractState(state)
        if (state.isFunded && currentTab == CurrentTab.EXTERNAL_FUNDING) {
            setDisplayVotingSlider(true)
        } else if (!state.isFunded && currentTab == CurrentTab.EXTERNAL_FUNDING)
            setDisplayVotingSlider(false)
    }

    internal suspend fun getBalance(address: PayAddress): Double {
        val balanceSatoshis = electrumAPI.getBalance(address)
        val balanceSatoshisTotal = (balanceSatoshis.unconfirmed + balanceSatoshis.confirmed).toDouble()
        return (balanceSatoshisTotal) / _satoshiRatio
    }

    private fun voteUsingVotePeerFunds(voteOption: VoteOption) {
        viewModelScope.launch(dispatcher + handler) {
            voteRepository.vote(voteOption)
            viewModelScope.launch(Dispatchers.Main) {
                _hasVotedState.value = true
                _hasVotedState.value = false
            }
        }
    }

    private suspend fun getCurrentUserBalance(): Double {
        val currentUserAddress = identityRepository.currentUser.address ?: throw IllegalStateException("Not authenticated.")
        val balanceSatoshis = electrumAPI.getBalance(currentUserAddress)
        val balanceSatoshisTotal = (balanceSatoshis.unconfirmed + balanceSatoshis.confirmed).toDouble()
        return (balanceSatoshisTotal) / _satoshiRatio
    }

    private fun voteFundedExternally(voteOption: VoteOption, funds: Array<PayloadContractUtxo>) {
        viewModelScope.launch(dispatcher + handler) {
            voteRepository.voteFundedContract(voteOption, funds)
            viewModelScope.launch(Dispatchers.Main + handler) {
                _hasVotedState.value = true
                _hasVotedState.value = false
            }
        }
    }

    private fun setDisplayVotingSlider(display: Boolean) = viewModelScope.launch(Dispatchers.Main) {
        _slideToVoteState.value = display
    }

    internal fun setContractState(state: ContractState) = viewModelScope.launch(Dispatchers.Main) {
        _contractState.value = state
    }

    internal fun bindIntents(view: FundVoteView) {
        view.initExceptionState().onEach {
            _exceptionState.filterNotNull().onEach {
                view.renderException(it)
            }.launchIn(viewModelScope + handler)
        }.launchIn(viewModelScope + handler)

        view.initSlideToVoteState().onEach {
            _slideToVoteState.filterNotNull().onEach {
                view.renderSlideToVote(it)
            }.launchIn(viewModelScope + handler)
        }.launchIn(viewModelScope + handler)

        view.initHasVotedState().onEach {
            _hasVotedState.filterNotNull().onEach {
                view.renderHasVotedState(it)
            }.launchIn(viewModelScope + handler)
        }.launchIn(viewModelScope + handler)
    }

    @DelicateCoroutinesApi
    class FundVoteViewModelFactory(
        private val dispatcher: CoroutineDispatcher,
        private val voteRepository: RingSignatureVoteRepository,
        private val electrumApi: ElectrumAPI,
        private val voteOption: VoteOption,
        private val identityRepository: IdentityRepository,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(FundVoteViewModel::class.java))
                return FundVoteViewModel(
                    dispatcher,
                    voteRepository,
                    electrumApi,
                    voteOption,
                    identityRepository,
                ) as T
            else
                throw IllegalArgumentException("ViewModel class must be a subclass of ElectionDetailViewModel!")
        }
    }
}