package info.bitcoinunlimited.votepeer.election.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.databinding.FragmentElectionDetailBinding
import info.bitcoinunlimited.votepeer.election.*
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import info.bitcoinunlimited.votepeer.utils.*
import info.bitcoinunlimited.votepeer.utils.Constants.EXPLORER_URL
import info.bitcoinunlimited.votepeer.vote.VoteOption
import kotlinx.coroutines.*

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailFragment : Fragment() {
    private var _binding: FragmentElectionDetailBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!
    private val safeArgs: ElectionDetailFragmentArgs by navArgs()
    private val viewModel: ElectionDetailViewModel by viewModels {
        InjectorUtils.provideElectionDetailViewModelFactory(
            safeArgs.privateKeyHex,
            safeArgs.election,
            safeArgs.currentBlockHeight,
            RingSignatureVoteDatabase.getInstance(requireContext()).ringSignatureVoteDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentElectionDetailBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.electionDetailView.apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val error: Throwable? by viewModel.error.collectAsState()
                val loading: Boolean by viewModel.loading.collectAsState()
                val selectedOption: String? by viewModel.selectedOption.collectAsState()
                val voted: Boolean? by viewModel.voted.collectAsState()
                val voteUrl: Uri? by viewModel.voteUrl.collectAsState()
                val lifecycle: ElectionLifecycle? by viewModel.lifecycle.collectAsState()
                val election: Election = viewModel.election
                ElectionScreen(loading, error, election, selectedOption, voteUrl, voted, lifecycle)
            }
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        viewModel.initialize()
    }

    @Composable
    fun ElectionScreen(
        loading: Boolean,
        error: Throwable?,
        election: Election,
        selectedOption: String?,
        voteUrl: Uri?,
        voted: Boolean?,
        lifecycle: ElectionLifecycle?
    ) {
        val hasVoted = when (voted) {
            true -> false
            false -> true
            null -> false
        }
        val votingAvailable = (hasVoted && lifecycle == ElectionLifecycle.ONGOING)
        val scrollState = rememberScrollState()
        Column(
            modifier = Modifier
                .verticalScroll(scrollState)
        ) {
            ElectionProgressBar(loading)
            ElectionError(error)
            ElectionDescriptionText(election.description)
            Divider(modifier = Modifier.padding(top = 8.dp, bottom = 8.dp))
            ElectionRadioGroup(selectedOption, election.options.toList(), votingAvailable) {
                viewModel.updateSelectedOption(it)
            }
            if (election is MultiOptionVoteElection)
                SubmitVoteButton(votingAvailable) {
                    if (selectedOption != null) viewModel.vote(selectedOption)
                    else throw Exception("Select an option to vote!")
                }
            else if (election is RingSignatureVoteElection)
                FundVoteButton(votingAvailable) {
                    if (selectedOption != null) navigateToFundFragment(selectedOption)
                    else throw Exception("Select an option to fund a vote!")
                }
            BlockchainExplorerButton(voteUrl) { openUrlInBrowser(it) }
            ElectionResultButton { navigateToElectionResult() }
            ElectionLifecycleText(lifecycle)
            HasVotedText(voted, lifecycle)
            when (election) {
                is RingSignatureVoteElection -> ElectionPrivacyEyeContainer()
                is MultiOptionVoteElection -> ElectionEyeContainer()
            }
        }
    }

    private fun navigateToElectionResult() {
        val electionResultDirections = ElectionDetailFragmentDirections
            .actionElectionDetailFragmentToElectionResultFragment(
                safeArgs.election.getTitle(),
                safeArgs.election,
                safeArgs.privateKeyHex
            )
        findNavController().navigate(electionResultDirections)
    }

    @Composable
    fun FundVoteButton(votingAvailable: Boolean, onClick: () -> Unit) {
        Button(
            enabled = votingAvailable,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp),
            onClick = onClick
        ) {
            Text("Fund Vote")
        }
    }

    private fun openUrlInBrowser(url: Uri) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = url
        startActivity(intent)
    }

    private fun navigateToFundFragment(selectedOption: String?) {
        if (selectedOption != null) {
            val election = safeArgs.election as RingSignatureVoteElection
            val electionResultDirections = ElectionDetailFragmentDirections
                .actionNavElectionDetailToFundFragment(
                    election,
                    safeArgs.currentBlockHeight,
                    safeArgs.privateKeyHex,
                    VoteOption(selectedOption),
                )
            findNavController().navigate(electionResultDirections)
        } else throw Exception("Select an option to fund vote!")
    }

    @Preview(showBackground = true)
    @Composable
    fun ElectionScreenPreview() {
        val participantsMock = ElectionFaker.participants.map { it.toByteArray() }.toTypedArray()
        val election = MultiOptionVoteElection(ElectionFaker.electionRawMock, participantsMock)
        val voteUrl = Uri.parse(EXPLORER_URL + "fakeId")
        ElectionScreen(true, Throwable("Something went wrong!!11"), election, election.options.first(), voteUrl, true, null)
    }
}

@Composable
fun ElectionProgressBar(loading: Boolean) {
    if (loading)
        LinearProgressIndicator(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .testTag("loading_tally")
        )
}

@Composable
fun ElectionError(error: Throwable?) {
    if (error != null) {
        Text(
            text = error.message ?: "Something went wrong!",
            fontSize = 14.sp,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(colorResource(id = R.color.colorAccent)),
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun ElectionRadioGroup(
    selectedOption: String?,
    options: List<String>,
    enabled: Boolean,
    onOptionSelected: (String) -> Unit
) {
    Column(
        modifier = Modifier
            .padding(start = 16.dp, end = 16.dp)
    ) {
        options.forEach { option ->
            Row(
                Modifier
                    .selectable(
                        selected = (option == selectedOption),
                        onClick = { onOptionSelected(option) },
                        enabled = enabled
                    )
                    .fillMaxWidth()
            ) {
                RadioButton(
                    selected = (option == selectedOption),
                    onClick = { onOptionSelected(option) },
                    modifier = Modifier
                        .width(24.dp),
                    enabled = enabled
                )
                Text(
                    text = option,
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .align(Alignment.CenterVertically),
                )
            }
        }
    }
}

@Composable
fun ElectionLifecycleText(lifecycle: ElectionLifecycle?) {
    val lifecycleText: String? = when (lifecycle) {
        ElectionLifecycle.NOT_STARTED -> "⌛ Election has not started yet!"
        ElectionLifecycle.ENDED -> stringResource(id = R.string.election_ended_vote_expired)
        else -> null
    }
    if (lifecycleText != null)
        Text(
            text = lifecycleText,
            modifier = Modifier
                .wrapContentWidth()
                .wrapContentHeight()
                .padding(16.dp),
            style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Normal)
        )
}

@Composable
fun SubmitVoteButton(enabled: Boolean, onClick: () -> Unit) {
    Button(
        onClick = onClick,
        enabled = enabled,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp)
    ) {
        Text(text = stringResource(R.string.submit_vote))
    }
}

@Composable
fun BlockchainExplorerButton(
    blockchainExplorerUrl: Uri?,
    onClick: (Uri) -> Unit
) {
    blockchainExplorerUrl?.let {
        Button(
            onClick = { onClick(blockchainExplorerUrl) },
            enabled = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp)
        ) {
            Text(stringResource(id = R.string.view_in_block_explorer))
        }
    }
}

@Composable
fun HasVotedText(voted: Boolean?, lifecycle: ElectionLifecycle?) {
    voted?.let {
        if (voted)
            Text(
                text = "✅ You have voted!",
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )
        else if (!voted && lifecycle == ElectionLifecycle.ONGOING)
            Text(
                text = stringResource(id = R.string.you_have_not_voted),
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )
        else if (!voted && lifecycle == ElectionLifecycle.ENDED)
            Text(
                text = stringResource(id = R.string.did_not_vote),
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )
    }
}

@Composable
fun ElectionResultButton(onClick: () -> Unit) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp)
    ) {
        Text("View results")
    }
}

@Composable
fun ElectionDescriptionText(description: String) {
    Text(
        text = description,
        modifier = Modifier
            .wrapContentWidth()
            .wrapContentHeight()
            .padding(16.dp),
        style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Normal)
    )
}

@Composable
fun ElectionPrivacyEyeContainer() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp, start = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        // ElectionPrivacyEye
        Image(
            painter = painterResource(R.drawable.ic_eye_line),
            contentDescription = stringResource(R.string.privacy),
            modifier = Modifier
                .size(40.dp)
        )

        // ElectionPrivacyEyeText
        Text(
            text = stringResource(R.string.anonymous_election_votes_are_not_traceable_to_identities_addresses),
            modifier = Modifier
                .weight(1f)
                .padding(end = 24.dp)
        )
    }
}

@Composable
fun ElectionEyeContainer() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp, start = 16.dp, end = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        // ElectionEye
        Image(
            painter = painterResource(R.drawable.ic_eye),
            contentDescription = stringResource(R.string.privacy),
            modifier = Modifier
                .size(width = 50.dp, height = 40.dp)
        )

        // TextElectionEye
        Text(
            text = stringResource(R.string.publicly_readable),
            modifier = Modifier
                .weight(1f)
                .padding(start = 8.dp)
        )
    }
}