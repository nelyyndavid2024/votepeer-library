package info.bitcoinunlimited.votepeer.election.tally

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class TallyIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val initStateError: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val refreshResults: MutableStateFlow<Event<RefreshResults>> = MutableStateFlow(Event(RefreshResults))
) {
    object RefreshResults
}
