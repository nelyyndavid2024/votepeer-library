package info.bitcoinunlimited.votepeer.utils

import android.app.Application
import androidx.fragment.app.Fragment
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.MultiOptionVoteRepository
import info.bitcoinunlimited.votepeer.VoteRepository
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.auth.HttpsProviderFirebase
import info.bitcoinunlimited.votepeer.election.*
import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailViewModel
import info.bitcoinunlimited.votepeer.election.fund.FundVoteViewModel
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewModelFactory
import info.bitcoinunlimited.votepeer.election.network.ElectionProviderRest
import info.bitcoinunlimited.votepeer.election.tally.TallyViewModel
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDao
import info.bitcoinunlimited.votepeer.twoOptionVote.TwoOptionVoteRepository
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivityViewModelFactory
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
object InjectorUtils {
    val chain = Constants.CURRENT_BLOCKCHAIN
    val dispatcher = Dispatchers.IO

    fun provideElectionMasterViewModelFactory(
        fragment: Fragment,
        payDestination: PayDestination,
        application: Application,
    ): ElectionMasterViewModelFactory {
        val electrumAPI = ElectrumAPI.getInstance(chain)
        val identityRepository = IdentityRepository.getInstance(payDestination, Wallet.Companion, Codec.Companion)
        val authRepository = AuthRepository.getInstance(identityRepository, httpsProviderFirebase = HttpsProviderFirebase)
        val electionService = ElectionService.getInstance(chain)

        return ElectionMasterViewModelFactory(
            fragment,
            electionService,
            authRepository,
            electrumAPI,
            ElectionProviderRest,
            chain,
            application
        )
    }

    fun provideFundVoteViewModelFactory(
        privateKeyHex: String,
        election: RingSignatureVoteElection,
        ringSignatureVoteDao: RingSignatureVoteDao,
        voteOption: VoteOption,

    ): FundVoteViewModel.FundVoteViewModelFactory {
        val secret = UnsecuredSecret(UtilStringEncoding.hexToByteArray(privateKeyHex))
        val payDestination: PayDestination = Pay2PubKeyHashDestination(chain, secret)
        val electrumAPI = ElectrumAPI.getInstance(chain)
        val identityRepository = IdentityRepository.getInstance(payDestination, Wallet.Companion, Codec.Companion)
        val electionService = ElectionService.getInstance(chain)
        val voteRepository = RingSignatureVoteRepository(
            election,
            electionService,
            electrumAPI,
            ringSignatureVoteDao,
            identityRepository,
        )

        return FundVoteViewModel.FundVoteViewModelFactory(dispatcher, voteRepository, electrumAPI, voteOption, identityRepository)
    }

    fun provideElectionDetailViewModelFactory(
        privateKeyHex: String,
        election: Election,
        currentBlockHeight: Long,
        ringSignatureVoteDao: RingSignatureVoteDao
    ): ElectionDetailViewModel.ElectionDetailViewModelFactory {
        val voteRepository = getVoteRepository(election, ringSignatureVoteDao, privateKeyHex)
        val electrumAPI = ElectrumAPI.getInstance(chain)

        return ElectionDetailViewModel.ElectionDetailViewModelFactory(
            dispatcher,
            election,
            electrumAPI,
            currentBlockHeight,
            ringSignatureVoteDao,
            voteRepository,
        )
    }

    fun getVoteRepository(election: Election, ringSignatureVoteDao: RingSignatureVoteDao, privateKeyHex: String): VoteRepository {
        val secret = UnsecuredSecret(UtilStringEncoding.hexToByteArray(privateKeyHex))
        val electrumAPI = ElectrumAPI.getInstance(chain)
        val payDestination: PayDestination = Pay2PubKeyHashDestination(chain, secret)
        val identityRepository = IdentityRepository.getInstance(payDestination, Wallet.Companion, Codec.Companion)
        val electionService = ElectionService.getInstance(chain)
        return when (election) {
            is RingSignatureVoteElection -> RingSignatureVoteRepository(
                election,
                electionService,
                electrumAPI,
                ringSignatureVoteDao,
                identityRepository,
            )
            is TwoOptionVoteElection -> TwoOptionVoteRepository(
                chain,
                election,
                electionService,
                electrumAPI,
                identityRepository,
            )
            is MultiOptionVoteElection -> MultiOptionVoteRepository(
                chain,
                election,
                electionService,
                electrumAPI,
                identityRepository,
            )
            else -> throw NotImplementedError(
                "Election type not supported in getVoteRepository"

            )
        }
    }

    fun getVoteRepository(election: Election, ringSignatureVoteDao: RingSignatureVoteDao, payDestination: PayDestination): VoteRepository {
        val electionService = ElectionService.getInstance(chain)
        val electrumAPI = ElectrumAPI.getInstance(chain)
        val identityRepository = IdentityRepository.getInstance(payDestination)

        return when (election) {
            is RingSignatureVoteElection -> RingSignatureVoteRepository(
                election,
                electionService,
                electrumAPI,
                ringSignatureVoteDao,
                identityRepository,
            )
            is TwoOptionVoteElection -> TwoOptionVoteRepository(
                chain,
                election,
                electionService,
                electrumAPI,
                identityRepository,
            )
            is MultiOptionVoteElection -> MultiOptionVoteRepository(
                chain,
                election,
                electionService,
                electrumAPI,
                identityRepository,
            )
            else -> throw NotImplementedError(
                "Election type not supported in getVoteRepository"
            )
        }
    }

    fun provideElectionResultViewModelFactory(
        privateKeyHex: String,
        election: Election,
        ringSignatureVoteDao: RingSignatureVoteDao
    ): TallyViewModel.TallyViewModelFactory {
        val secret = UnsecuredSecret(UtilStringEncoding.hexToByteArray(privateKeyHex))
        val payDestination: PayDestination = Pay2PubKeyHashDestination(chain, secret)
        val identityRepository = IdentityRepository.getInstance(payDestination, Wallet.Companion, Codec.Companion)
        val voteRepository = getVoteRepository(election, ringSignatureVoteDao, payDestination)

        return TallyViewModel.TallyViewModelFactory(
            identityRepository,
            election,
            voteRepository,
        )
    }

    fun provideVotePeerActivityViewModelFactory(
        application: Application,
        owner: SavedStateRegistryOwner,
        privateKey: ByteArray
    ): VotePeerActivityViewModelFactory {
        val secret = UnsecuredSecret(privateKey)
        val payDestination: PayDestination = Pay2PubKeyHashDestination(chain, secret)
        val electrumAPI = ElectrumAPI.getInstance(chain)
        val identityRepository = IdentityRepository.getInstance(payDestination, Wallet.Companion, Codec.Companion)
        val authRepository = AuthRepository.getInstance(identityRepository, httpsProviderFirebase = HttpsProviderFirebase)

        return VotePeerActivityViewModelFactory(authRepository, electrumAPI, owner, chain, application)
    }
}
