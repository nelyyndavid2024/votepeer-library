package info.bitcoinunlimited.votepeer

import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.auth.network.LinkPublicKeyRequest
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.utils.TAG_IDENTITY_REPOSITORY
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
class IdentityRepository(
    val currentUser: PayDestination,
    private val walletCompanion: Wallet.Companion,
    private val codecCompanion: Codec.Companion,
) {
    private val privateKey = currentUser.secret ?: throw Exception("Private-key missing!")
    private val address = currentUser.address ?: throw Exception("Cannot get address")

    fun getPkh(): ByteArray {
        return currentUser.pkh() ?: throw Exception("$TAG_IDENTITY_REPOSITORY Cannot get currentUserPkh")
    }

    fun signChallenge(challenge: String): String {
        val challengeBytes = challenge.toByteArray()
        val signedChallenge = walletCompanion.signMessage(challengeBytes, privateKey.getSecret())
        return codecCompanion.encode64(signedChallenge)
    }

    fun getAddress(): String {
        return address.toString()
    }

    internal fun getLinkPublicKeyRequest(
        chain: ChainSelector,
    ): LinkPublicKeyRequest {
        val secretKey = privateKey.getSecret()
        // Use bitcoin privateKey as seed for ring signature keypair
        val keypair = RingSignature.newKeyPairFromBits(secretKey)
        val ringSigPubKey = RingSignature.getPubKey(keypair)
        val ringSigPubkeyHex = UtilStringEncoding.toHexString(ringSigPubKey)
        val message = "link:$ringSigPubkeyHex"
        val signedMessage = Wallet.Companion.signMessage(message.toByteArray(), secretKey)
        val signedMessage64 = Codec.encode64(signedMessage)
        val address = Pay2PubKeyHashDestination(chain, privateKey).address.toString()

        return LinkPublicKeyRequest(
            userid = address,
            purpose = "ringsignature",
            pubkey = ringSigPubkeyHex,
            signature = signedMessage64
        )
    }

    companion object {
        const val satoshiRatio = 100000000.0
        fun createPayDestination(privateKeyHex: String): PayDestination {
            val privateKey = UtilStringEncoding.hexToByteArray(privateKeyHex)
            val secret = UnsecuredSecret(privateKey)
            return Pay2PubKeyHashDestination(Constants.CURRENT_BLOCKCHAIN, secret)
        }

        // For Singleton instantiation
        @Volatile
        private var instance: IdentityRepository? = null

        fun getInstance(
            currentUser: PayDestination,
            walletCompanion: Wallet.Companion = Wallet.Companion,
            codecCompanion: Codec.Companion = Codec.Companion,
        ) = instance ?: synchronized(this) {
            instance ?: IdentityRepository(
                currentUser,
                walletCompanion,
                codecCompanion,
            ).also { instance = it }
        }
    }
}