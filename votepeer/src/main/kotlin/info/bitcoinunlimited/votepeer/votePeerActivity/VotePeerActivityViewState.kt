package info.bitcoinunlimited.votepeer.votePeerActivity

import info.bitcoinunlimited.votepeer.election.detail.ElectionDetailFragmentArgs

sealed class VotePeerActivityViewState {
    object DefaultNone : VotePeerActivityViewState()

    data class ConnectionStatus(
        val isOnline: Boolean
    ) : VotePeerActivityViewState()

    object StartQrScanner : VotePeerActivityViewState()

    data class Notification(
        val electionDetailFragmentArgs: ElectionDetailFragmentArgs
    ) : VotePeerActivityViewState()

    data class VotePeerActivityError(
        val exception: Exception
    ) : VotePeerActivityViewState()
}
