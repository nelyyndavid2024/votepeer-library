package info.bitcoinunlimited.votepeer.votePeerActivity

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import bitcoinunlimited.libbitcoincash.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.google.zxing.integration.android.IntentIntegrator
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterFragmentDirections
import info.bitcoinunlimited.votepeer.utils.*
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
open class VotePeerActivity : AppCompatActivity(), VotePeerActivityView {
    protected val viewModel: VotePeerActivityViewModel by viewModels {
        InjectorUtils.provideVotePeerActivityViewModelFactory(
            application ?: throw IllegalStateException("Cannot get application in VotePeerActivity!"),
            this,
            intent.getByteArrayExtra(Constants.PRIVATE_KEY)
                ?: throw Exception("Cannot get privateKey from intent")
        )
    }

    private val votingActivityViewIntent = VotePeerActivityViewIntent()

    override fun initState() = votingActivityViewIntent.initState
    override fun qrCodeRead() = votingActivityViewIntent.qrCodeRead
    override fun submitConnectionStatus() = votingActivityViewIntent.submitConnectionStatus

    fun getPayDestination(intent: Intent): PayDestination {
        val privateKey = intent.getByteArrayExtra(Constants.PRIVATE_KEY) ?: throw Exception("Cannot get privateKey from intent")
        val chain = intent.getByteArrayExtra("chain") as ChainSelector? ?: ChainSelector.BCHMAINNET
        return Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKey))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeConnectionStatus()
        viewModel.bindIntents(this)
        // TODO: Move to init in viewModel
        viewModel.initAuth()
        viewModel.initBlockchainConnection()
        initLayout()
    }

    private fun initLayout(
        setSupportActionBar: Boolean = intent.getBooleanExtra("setSupportActionBar", true),
        setupActionBarWithNavController: Boolean = intent.getBooleanExtra(Constants.SETUP_ACTION_BAR_WITH_NAV_CONTROLLER, true),
        setContentView: Boolean = intent.getBooleanExtra("setContentView", true),
        topLevelDestinations: Set<Int> = getTopLevelDestinations(),
    ) {
        if (setContentView) {
            val navView: NavigationView = findViewById(R.id.nav_view_votepeer_lib)
            setContentView(R.layout.activity_pure_voting_votepeer_lib)
            val appBarConfiguration = AppBarConfiguration(
                topLevelDestinations,
            )
            val navController = findNavController(R.id.nav_host_fragment_votepeer_lib)
            if (setupActionBarWithNavController) setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)

            findViewById<Toolbar>(R.id.toolbar_votepeer_lib).setNavigationOnClickListener {
                hideKeyBoard()
                navController.popBackStack()
            }
        }
        if (setSupportActionBar) {
            val toolbar: Toolbar? = findViewById(R.id.toolbar_votepeer_lib)
            setSupportActionBar(toolbar)
        }
    }

    // TODO: Move to activityViewModel: Hide/Show state
    private fun hideKeyBoard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager ?: return
        imm.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    override fun onResume() {
        super.onResume()
        val toolbar: TextView? = findViewById(R.id.toolbar_error)
        toolbar?.visibility = View.GONE
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun observeConnectionStatus() {
        val isOnline = isOnline(applicationContext)
        val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        votingActivityViewIntent.submitConnectionStatus.value = Event(VotePeerActivityViewState.ConnectionStatus(isOnline))
        connectivityManager.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                votingActivityViewIntent.submitConnectionStatus.value = Event(VotePeerActivityViewState.ConnectionStatus(true))
            }

            override fun onLost(network: Network) {
                votingActivityViewIntent.submitConnectionStatus.value = Event(VotePeerActivityViewState.ConnectionStatus(false))
            }
        })
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    override fun render(state: VotePeerActivityViewState) {
        when (state) {
            is VotePeerActivityViewState.DefaultNone -> {} // Do nothing
            is VotePeerActivityViewState.ConnectionStatus -> renderConnectionStatus(state)
            is VotePeerActivityViewState.VotePeerActivityError -> { renderError(state) }
            is VotePeerActivityViewState.StartQrScanner -> { renderQrScannerState() }
            is VotePeerActivityViewState.Notification -> renderNotification(state)
        }
    }

    private fun renderQrScannerState() {
        IntentIntegrator(this).initiateScan()
    }

    private fun renderNotification(state: VotePeerActivityViewState.Notification) {
        val args = state.electionDetailFragmentArgs
        val action = ElectionMasterFragmentDirections.actionElectionMasterFragmentToElectionDetailFragment(
            args.electionTitle,
            args.election,
            args.currentBlockHeight,
            args.privateKeyHex
        )
        findNavController(R.id.nav_graph_election).navigate(action)
    }

    private fun renderConnectionStatus(connection: VotePeerActivityViewState.ConnectionStatus) {
        val isConnected = connection.isOnline
        val toolbar: TextView? = findViewById(R.id.toolbar_error)
        val electrumApiConnectionStatus: TextView? = findViewById(R.id.toolbar_electrum_api_connection_status)

        if (isConnected) {
            toolbar?.visibility = View.GONE
            electrumApiConnectionStatus?.visibility = View.VISIBLE
        } else {
            toolbar?.visibility = View.VISIBLE
            toolbar?.text = getString(R.string.offline)
            electrumApiConnectionStatus?.visibility = View.GONE
        }
    }

    // TODO: Move to text-view on the top of screen with "ok"-button?
    private fun renderError(state: VotePeerActivityViewState.VotePeerActivityError) {
        val exception = state.exception
        val message = exception.localizedMessage ?: exception.message ?: ""
        Log.e(TAG_VOTEPEER_ACTIVITY, message)
        MaterialAlertDialogBuilder(this)
            .setTitle("Something went wrong in MainActivity!")
            .setMessage(message)
            .setNeutralButton("ok") { _, _ ->
                // Do nothing
            }
            .show()
    }

    override fun onSupportNavigateUp(): Boolean {
        val setContentView = intent.getBooleanExtra("setContentView", true)
        if (setContentView) {
            val navController = findNavController(R.id.nav_host_fragment_votepeer_lib)
            return navController.navigateUp() || super.onSupportNavigateUp()
        }
        return false
    }

    companion object {
        @Suppress("unused")
        fun getTopLevelDestinations(): Set<Int> {
            return setOf(R.id.nav_election_master)
        }
    }
}
