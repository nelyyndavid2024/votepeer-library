package info.bitcoinunlimited.votepeer.auth

import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.utils.TAG_AUTH_REPOSITORY
import info.bitcoinunlimited.votepeer.votePeerActivity.QrCodeData
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.tasks.await

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class AuthRepository(
    val identityRepository: IdentityRepository,
    private val authProviderFirebase: AuthProviderFirebase,
    private val httpsProviderFirebase: HttpsProvider,
    private val restProviderAuth: RestProviderAuth,
    initialAuthState: AuthState
) {
    private val _authState = MutableStateFlow(initialAuthState)
    val authState = _authState.asStateFlow()
    private val handler = CoroutineExceptionHandler { _, throwable ->
        _authState.value = AuthState.AuthError(Exception(throwable))
    }
    init {
        GlobalScope.launch(Dispatchers.IO + handler) {
            observeFirebaseAuthState()
        }
    }

    // TODO: Move to AuthServiceProviderFirebase
    private fun observeFirebaseAuthState() {
        FirebaseAuth.getInstance().addAuthStateListener {
            val state = AuthProviderFirebase.fetchAuthState(it)
            _authState.value = state
        }
    }

    @Suppress("unused")
    fun logout() {
        return authProviderFirebase.signOut()
    }

    private suspend fun fetchChallenge(): String? {
        return try {
            httpsProviderFirebase.getChallenge()
        } catch (exception: Exception) {
            _authState.value = AuthState.AuthError(exception)
            Log.e(TAG_AUTH_REPOSITORY, exception.message ?: exception.toString())
            null
        }
    }

    fun getCurrentUserId(): String? {
        return authProviderFirebase.getCurrentUserId()
    }

    private suspend fun identifyLogin(cookie: String, signedChallenge: String): String? {
        val address = identityRepository.getAddress()
        return try {
            val response = restProviderAuth.identify(
                cookie,
                address,
                signedChallenge,
            )
            return response.token
        } catch (error: Exception) {
            _authState.value = AuthState.AuthError(error)
            null
        }
    }

    suspend fun signInToWebsite(qrData: QrCodeData) {
        val signedChallenge = identityRepository.signChallenge(qrData.chal)
        identifyLogin(qrData.cookie, signedChallenge)
    }

    suspend fun signInWithKeyPair() {
        _authState.value = AuthState.Unauthenticated
        authProviderFirebase.signOut()
        val anonymousUserID = authProviderFirebase.signInAnonymously()
        _authState.value = AuthState.AuthenticatedAnonymously(anonymousUserID)
        val challenge: String = fetchChallenge() ?: throw Exception("Cannot get challenge")
        val cookie: String = authProviderFirebase.getCookie() ?: throw Exception("Cannot get cookie")
        val signedChallenge = identityRepository.signChallenge(challenge)
        val jsonWebToken: String = identifyLogin(cookie, signedChallenge) ?: return
        try {
            val userId = authProviderFirebase.signInWithJsonWebToken(jsonWebToken)
            _authState.value = AuthState.AuthenticatedKeyPair(userId)
        } catch (error: Exception) {
            _authState.value = AuthState.AuthError(error)
        }
    }

    @Suppress("MemberVisibilityCanBePrivate")
    suspend fun setAndroidNotificationToken(firebaseMessagingToken: String) {
        val currentUserId = identityRepository.getAddress()
        if (authState.value is AuthState.AuthenticatedKeyPair) {
            val data = hashMapOf("androidNotificationToken" to firebaseMessagingToken)
            Firebase.firestore.collection("user").document(currentUserId).set(data, SetOptions.merge()).await()
        }
    }

    // TODO: Move to messagingService?
    suspend fun updateNotificationToken() {
        try {
            val token = FirebaseMessaging.getInstance().token.await()
            setAndroidNotificationToken(token)
        } catch (error: Exception) {
            Log.w(TAG_AUTH_REPOSITORY, "Fetching FCM registration token failed", error)
            return
        }
    }

    // Link ring signature public key to enable anonymous voting for user.
    // We use private key of user as seed for the ring signature keypair.
    suspend fun linkPublicKey(chain: ChainSelector): String {
        val request = identityRepository.getLinkPublicKeyRequest(chain)
        return restProviderAuth.linkPublicKey(request).status
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: AuthRepository? = null

        fun getInstance(
            identityRepository: IdentityRepository,
            authServiceProviderFirebase: AuthProviderFirebase = AuthProviderFirebase,
            httpsProviderFirebase: HttpsProviderFirebase = HttpsProviderFirebase,
            restProviderAuth: RestProviderAuth = RestProviderAuth,
            initialAuthState: AuthState = AuthState.Unauthenticated
        ) = instance ?: synchronized(this) {
            instance
                ?: AuthRepository(
                    identityRepository,
                    authServiceProviderFirebase,
                    httpsProviderFirebase,
                    restProviderAuth,
                    initialAuthState
                )
                    .also { instance = it }
        }
    }
}
