package info.bitcoinunlimited.votepeer.auth.network

data class GetLinkedKeysRequest(
    val election_id: String,
)
