package info.bitcoinunlimited.votepeer.auth.network

import kotlinx.serialization.Serializable

@Serializable
data class JsonWebTokenResponse(
    val token: String,
)
