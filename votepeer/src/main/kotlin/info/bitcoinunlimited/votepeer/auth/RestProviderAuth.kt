package info.bitcoinunlimited.votepeer.auth

import info.bitcoinunlimited.votepeer.auth.network.*
import info.bitcoinunlimited.votepeer.utils.Constants
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.DelicateCoroutinesApi

// TODO: Mock and tests
@DelicateCoroutinesApi
object RestProviderAuth {
    internal suspend fun identify(
        cookie: String,
        address: String,
        signedChallenge: String,
    ): JsonWebTokenResponse {
        val operation = "login"
        val httpclient = RestServiceAuth.createHttpClient()
        val jsonWebTokenResponse: JsonWebTokenResponse = httpclient.get(
            "${Constants.EndpointUrl}identify"
        ) {
            parameter("op", operation)
            parameter("cookie", cookie)
            parameter("addr", address)
            parameter("sig", signedChallenge)
        }
        return jsonWebTokenResponse
    }

    internal suspend fun linkPublicKey(request: LinkPublicKeyRequest): LinkPublicKeyResponse {
        val httpclient = RestServiceAuth.createHttpClient()
        val url = "${Constants.EndpointUrl}link_public_key"
        val linkPublicKeyResponse: LinkPublicKeyResponse = httpclient.post(url) {
            contentType(ContentType.Application.Json)
            body = request
        }
        return linkPublicKeyResponse
    }
}