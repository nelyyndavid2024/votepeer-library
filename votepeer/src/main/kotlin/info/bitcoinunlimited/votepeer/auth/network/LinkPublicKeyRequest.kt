package info.bitcoinunlimited.votepeer.auth.network

import kotlinx.serialization.Serializable

@Serializable
data class LinkPublicKeyRequest(
    val userid: String,
    val purpose: String,
    val pubkey: String,
    val signature: String
)
