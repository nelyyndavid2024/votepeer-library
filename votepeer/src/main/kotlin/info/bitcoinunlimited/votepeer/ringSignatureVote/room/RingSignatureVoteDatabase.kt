package info.bitcoinunlimited.votepeer.ringSignatureVote.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [RingSignatureVoteRoom::class], version = 3)
@TypeConverters(RingSignatureVoteRoomConverters::class)
abstract class RingSignatureVoteDatabase : RoomDatabase() {
    abstract fun ringSignatureVoteDao(): RingSignatureVoteDao

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: RingSignatureVoteDatabase? = null

        fun getInstance(context: Context): RingSignatureVoteDatabase {
            return instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }
        }

        private fun buildDatabase(context: Context): RingSignatureVoteDatabase {
            return Room.databaseBuilder(
                context, RingSignatureVoteDatabase::class.java,
                "ring-signature-vote-db"
            ).fallbackToDestructiveMigration().build()
        }
    }
}