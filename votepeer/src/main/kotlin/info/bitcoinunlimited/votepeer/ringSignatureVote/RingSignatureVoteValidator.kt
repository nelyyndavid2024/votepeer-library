package info.bitcoinunlimited.votepeer.ringSignatureVote

import android.util.Log
import bitcoinunlimited.libbitcoincash.RingSignature
import bitcoinunlimited.libbitcoincash.TagPtr
import bitcoinunlimited.libbitcoincash.TraceResult
import info.bitcoinunlimited.votepeer.utils.TAG_RING_SIGNATURE_VOTE_VALIDATOR
import info.bitcoinunlimited.votepeer.vote.AcceptedVote

/**
 * For validating a vote payload. Verifies that it is correctly signed and not a duplicate vote.
 *
 * Usage:
 * ```
 * RingSignatureVoteValidator(electionID, participants).use {
 *   if (it.isAcceptable(vote, signature, alreadyAcceptedVotes)) {
 *      // do stuff
 *   }
 * }
 * ```
 */
class RingSignatureVoteValidator(electionID: ByteArray, participants: Array<ByteArray>) : AutoCloseable {
    private val tag: TagPtr = RingSignature.createTag(electionID, participants)

    override fun close() {
        RingSignature.freeTag(tag)
    }

    fun isAcceptable(vote: ByteArray, signature: ByteArray, alreadyAccepted: List<AcceptedVote>): Boolean {
        if (!RingSignature.verify(vote, tag, signature)) {
            val message = "RingSignatureVoteValidator.isAccapetable(...) verify failed"
            val exception = Exception(message)
            Log.d(TAG_RING_SIGNATURE_VOTE_VALIDATOR, message)
            throw exception
        }

        for (accepted in alreadyAccepted) {
            try {
                val (result, exposedPubkey) = RingSignature.trace(
                    message1 = vote,
                    signature1 = signature,
                    message2 = accepted.vote,
                    signature2 = accepted.signature,
                    tag
                )

                return when (result) {
                    TraceResult.Indep -> true
                    TraceResult.Revealed -> {
                        accepted.exposedOwner = exposedPubkey
                        Log.d(TAG_RING_SIGNATURE_VOTE_VALIDATOR, "Ringsig trace detected duplicate vote and exposed owner")
                        false
                    }
                    TraceResult.Linked -> {
                        Log.d(TAG_RING_SIGNATURE_VOTE_VALIDATOR, "Ringsig trace detected duplicate vote")
                        false
                    }
                }
            } catch (e: Error) {
                Log.i(TAG_RING_SIGNATURE_VOTE_VALIDATOR, "Vote validator trace failed: $e")
                return false
            } catch (e: Exception) {
                Log.i(TAG_RING_SIGNATURE_VOTE_VALIDATOR, "Vote validator trace failed: $e")
                return false
            }
        }
        return true
    }
}