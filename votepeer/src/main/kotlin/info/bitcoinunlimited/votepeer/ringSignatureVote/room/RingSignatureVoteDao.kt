package info.bitcoinunlimited.votepeer.ringSignatureVote.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RingSignatureVoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(ringSignatureVoteRoom: RingSignatureVoteRoom)

    @Query("SELECT * FROM ring_signature_vote_table WHERE electionId == :electionId")
    fun get(electionId: String): RingSignatureVoteRoom?

    @Query("SELECT transactionId FROM ring_signature_vote_table WHERE electionId == :electionId")
    fun getTransactionId(electionId: String): String?

    @Query("SELECT selectedOption FROM ring_signature_vote_table WHERE electionId == :electionId")
    // fun getSelectedOptionIndex(electionId: String): Int?
    fun getSelectedOption(electionId: String): String?
}
