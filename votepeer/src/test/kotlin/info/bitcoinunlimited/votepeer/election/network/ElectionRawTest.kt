package info.bitcoinunlimited.votepeer.election.network

import io.mockk.mockk
import java.io.Serializable
import org.junit.jupiter.api.Test

internal class ElectionRawTest {

    @Test
    fun isSerializableTest() {
        val electionRawMock: ElectionRaw = mockk()
        assert(electionRawMock is Serializable)
    }
}