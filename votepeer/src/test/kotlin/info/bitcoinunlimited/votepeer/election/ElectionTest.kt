package info.bitcoinunlimited.votepeer.election

import info.bitcoinunlimited.votepeer.ElectionUnitMocker
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class ElectionTest {
    private val electionRaw = ElectionUnitMocker.electionRawMock
    private val electionMock = ElectionUnitMocker.twoOptionElectionMock

    @Test
    fun getTitle() {
        val mockTitle = "Mock title"
        val electionMockWithMyDescription = electionMock.copy(electionRaw.copy(description = mockTitle))
        assertEquals(mockTitle, electionMockWithMyDescription.getTitle())
    }

    @Test
    fun getTitleOverSixtyCharacters() {
        val mockTitle = "Mock title that is over forty characters long. That is a lot of characters!"
        val electionMockWithMyDescription = electionMock.copy(electionRaw.copy(description = mockTitle))
        assertEquals("Mock title that is over forty characters long. That is a lot...", electionMockWithMyDescription.getTitle())
        assertNotEquals(mockTitle, electionMockWithMyDescription.getTitle())
    }

    @Test
    fun getEndTimeHumanNoBlockHeight() {
        val endTimeHumanMock = "⚠️ Ongoing! Ends in: 8 hour(s) and 20 minute(s) at block ${electionMock.endHeight}"
        val heightMock = 350L
        val lifecycleState = electionMock.getElectionLifecycleStateHuman(heightMock)
        assertEquals(endTimeHumanMock, lifecycleState)
    }

    @Test
    fun getEndTimeHumanEnded() {
        val heightMock = 401L
        val endTimeHuman = electionMock.getElectionLifecycleStateHuman(heightMock)
        assertEquals("\uD83C\uDFC1 Ended 10 minute(s) ago at block ${electionMock.endHeight}", endTimeHuman)
    }

    @Test
    fun getLifecycleStateAndEndTimeHuman() {
        val endTimeHumanMock = "⚠️ Ongoing! Ends in: 16 hour(s) and 30 minute(s) at block ${electionMock.endHeight}"
        val heightMock = 301L
        val endTimeHuman = electionMock.getElectionLifecycleStateHuman(heightMock)
        assertEquals(endTimeHumanMock, endTimeHuman)
    }

    @Test
    fun getTimeIntervalHumanWrongInputTest() {
        val startHeightMock = 1000L
        val endHeightMock = 100L
        val mockMessage = "startHeight: $startHeightMock must be less than endHeight: $endHeightMock"
        val exception = assertThrows<IllegalArgumentException> {
            electionMock.getTimeIntervalHuman(startHeightMock, endHeightMock)
        }
        assertEquals(mockMessage, exception.message)
    }

    @Test
    fun getHostedBy() {
        val hostedBy = electionMock.getHostedByHuman()
        assertEquals("Hosted by qp94x7qc0u...8rw0s0", hostedBy)
    }

    @Test
    fun getOptionHashOutOfBoundsTest() {
        val options = electionMock.options.size
        val optionIndex = options + 1
        // TODO("Replace with non-existant option?")
        /*
        val exception = assertThrows<IndexOutOfBoundsException> {
            electionMock.getOptionHash(optionIndex)
        }
        assertEquals(
            exception.message,
            "Option number $optionIndex out of bounds. " +
                "There are only $options options in this election"
        )
        */
    }

    @Test fun getElectionLifecycleStateCreatedTest() {
        val heightMock = 100L
        val electionState = electionMock.getElectionLifecycleState(heightMock)
        val expectedState = ElectionLifecycle.NOT_STARTED
        assertEquals(electionState, expectedState)
    }

    @Test fun getElectionLifecycleStateOngoingTest() {
        val heightMock = 350L
        val electionState = electionMock.getElectionLifecycleState(heightMock)
        val expectedState = ElectionLifecycle.ONGOING
        assertEquals(electionState, expectedState)
    }

    @Test fun getElectionLifecycleStateEndedTest() {
        val heightMock = 420L
        val electionState = electionMock.getElectionLifecycleState(heightMock)
        val expectedState = ElectionLifecycle.ENDED
        assertEquals(electionState, expectedState)
    }

    @Test fun getElectionLifecycleStateStateErrorTest() {
        val heightMock = 250L
        val electionWithErroneousBlockTimes = electionMock.copy(electionRaw.copy(beginHeight = 300L, endHeight = 100L))
        val exception = assertThrows<IllegalStateException> {
            electionWithErroneousBlockTimes.getElectionLifecycleState(heightMock)
        }
        assertEquals(
            exception.message,
            "Something is wrong: beginHeight: ${electionWithErroneousBlockTimes.beginHeight}," +
                " endHeight: ${electionWithErroneousBlockTimes.endHeight}, currentHeight: $heightMock"
        )
    }
}