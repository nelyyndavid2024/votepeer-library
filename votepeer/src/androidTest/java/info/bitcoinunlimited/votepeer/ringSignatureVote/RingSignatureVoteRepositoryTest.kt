package info.bitcoinunlimited.votepeer.ringSignatureVote

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.election.ElectionServiceTest
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewModelTest
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDao
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivityViewModelTest
import io.mockk.clearAllMocks
import io.mockk.spyk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.jupiter.api.*

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
class RingSignatureVoteRepositoryTest {
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()
    private val ringSignatureElection = ElectionFaker.generateOngoingRingSignatureElection("mock_id")
    private lateinit var ringSignatureVoteRepository: RingSignatureVoteRepository
    private lateinit var electionService: ElectionService
    private lateinit var electrumAPI: ElectrumAPI
    private lateinit var database: RingSignatureVoteDatabase
    private lateinit var dao: RingSignatureVoteDao
    private lateinit var currentUser: PayDestination

    companion object {
        private val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }

    @BeforeEach fun init() {
        val privateKey = ElectionFaker.mockPrivateKey('L')
        electionService = ElectionService.getInstance(ElectionServiceTest.chain)
        database = Room.inMemoryDatabaseBuilder(applicationContext, RingSignatureVoteDatabase::class.java).build()
        dao = database.ringSignatureVoteDao()
        currentUser = Pay2PubKeyHashDestination(ElectionMasterViewModelTest.chain, UnsecuredSecret(privateKey))
        electrumAPI = spyk(ElectrumAPI.getInstance(VotePeerActivityViewModelTest.chain))
        val identityRepository = IdentityRepository.getInstance(currentUser)
        ringSignatureVoteRepository = RingSignatureVoteRepository(
            ringSignatureElection,
            electionService,
            electrumAPI,
            dao,
            identityRepository
        )
    }

    @After
    fun closeDb() {
        database.close()
    }

    @AfterEach
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun getVoteOptionException() = runBlockingTest {
        val message = RingSignatureVoteRepository.getNotFoundMessage(ringSignatureElection)
        val exception = assertThrows<RingSignatureVoteNotFoundInRoomException> {
            ringSignatureVoteRepository.getVoteOption()
        }
        Assertions.assertEquals(exception.message, message)
    }
}