package info.bitcoinunlimited.votepeer.election.master

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.ToHexStr
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.Matchers.isRefreshing
import info.bitcoinunlimited.votepeer.Matchers.recyclerViewSizeMatcher
import info.bitcoinunlimited.votepeer.Matchers.withBackgroundColor
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.utils.Event
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import java.util.stream.Stream
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.not
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource

/*
    Must be imported into an app-project such as VotePeer locally to run
 */
@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
internal class ElectionMasterFragmentTest {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    private lateinit var scenario: FragmentScenario<ElectionMasterFragment>
    private lateinit var navController: TestNavHostController
    private val privateKey = ElectionFaker.mockPrivateKey()
    private val privateKeyHex = ToHexStr(privateKey)
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()

    @BeforeEach
    fun setUp() {
        mockkStatic(Firebase::class)
        mockkStatic(FirebaseApp::class)
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFunctions::class)
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { FirebaseFunctions.getInstance(Constants.region) } returns mockk(relaxed = true)
        navController = TestNavHostController(applicationContext)

        val fragmentArgs = bundleOf("privateKeyHex" to privateKeyHex)
        scenario = launchFragmentInContainer(fragmentArgs, R.style.AppTheme)
        scenario.onFragment { fragment ->
            navController.setGraph(R.navigation.nav_graph_election)
            navController.setCurrentDestination(R.id.nav_election_master)
            Navigation.setViewNavController(fragment.requireView(), navController)
        }
    }

    @AfterEach
    fun clear() {
        clearAllMocks()
    }

    @Test fun testInit() {
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(isDisplayed()))
        onView(withId(R.id.swipe_to_refresh)).check(matches(isDisplayed()))
        onView(withId(R.id.no_elections_bitcoin_unlimited_logo)).check(matches(isDisplayed()))
        onView(withId(R.id.no_elections_title)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_elections_content)).check(matches(not(isDisplayed())))

        onView(withId(R.id.election_master_recycler_view)).check(matches(recyclerViewSizeMatcher(0)))
        onView(withId(R.id.election_master_recycler_view)).check(matches(not(isDisplayed())))
    }
    @Test fun swipeToRefreshTest() {
        scenario.onFragment { fragment ->
            fragment.intent.swipeToRefresh.value = Event(ElectionMasterViewIntent.SwipeToRefresh)
        }
    }

    @Test fun loadingSpinnerTest() {
        scenario.onFragment { fragment ->
            val mockViewState = ElectionMasterViewState.LoadingSpinner(true)
            fragment.render(mockViewState)
        }
        onView(withId(R.id.swipe_to_refresh)).check(matches(isRefreshing()))

        scenario.onFragment { fragment ->
            val mockViewState = ElectionMasterViewState.LoadingSpinner(false)
            fragment.render(mockViewState)
        }
        onView(withId(R.id.swipe_to_refresh)).check(matches(not(isRefreshing())))
    }

    @Test fun noElectionsTest() {
        scenario.onFragment { fragment ->
            val mockViewState = ElectionsViewState(listOf())
            fragment.renderElections(mockViewState)
        }
        onView(withId(R.id.swipe_to_refresh)).check(matches(isDisplayed()))
        onView(withId(R.id.no_elections_bitcoin_unlimited_logo)).check(matches(isDisplayed()))
        onView(withId(R.id.no_elections_content)).check(matches(isDisplayed()))
        onView(withId(R.id.no_elections_title)).check(matches(isDisplayed()))
        onView(withId(R.id.open_voter_cash)).check(matches(isDisplayed()))

        onView(withId(R.id.election_master_recycler_view)).check(matches(recyclerViewSizeMatcher(0)))
        onView(withId(R.id.election_master_recycler_view)).check(matches(not(isDisplayed())))
    }

    internal class ElectionsTestParams : ArgumentsProvider {
        override fun provideArguments(context: ExtensionContext): Stream<out Arguments?> {
            return Stream.of(
                Arguments.of(ElectionFaker.generateTwoOptionElections(1)),
                Arguments.of(ElectionFaker.generateTwoOptionElections(2)),
                Arguments.of(ElectionFaker.generateTwoOptionElections(10)),
                Arguments.of(ElectionFaker.generateTwoOptionElections(25)),
                Arguments.of(ElectionFaker.generateTwoOptionElections(50)),
                Arguments.of(ElectionFaker.generateTwoOptionElections(100))
            )
        }
    }

    @Test fun renderSingleElectionTest() {
        val heightMock = 350L
        val election = ElectionFaker.generateOngoingTwoOptionElection(heightMock)
        scenario.onFragment { fragment ->
            election.voteSentence = "You voted for foobar"
            val mockViewState = ElectionsViewState(listOf(election))
            fragment.renderElections(mockViewState)
        }

        onView(withId(R.id.election_master_recycler_view)).check(matches(recyclerViewSizeMatcher(1)))
        onView(withId(R.id.election_master_recycler_view)).check(matches((isDisplayed())))

        onView(withId(R.id.election_master_recycler_view))
            .perform(RecyclerViewActions.scrollToPosition<ElectionMasterAdapter.ViewHolder>(1))
        onView(withText(election.getTitle())).check(matches(isDisplayed()))

        onView(withText(election.getTitle())).check(matches(isDisplayed()))
        onView(withText(election.voteSentence)).check(matches(isDisplayed()))
        onView(withText(election.getHostedByHuman())).check(matches(isDisplayed()))

        onView(withId(R.id.no_elections_bitcoin_unlimited_logo)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_elections_content)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_elections_title)).check(matches(not(isDisplayed())))
        onView(withId(R.id.open_voter_cash)).check(matches(not(isDisplayed())))
    }

    // TODO: Add test cases with different election lifecycle states.
    @ParameterizedTest
    @ArgumentsSource(ElectionsTestParams::class)
    fun renderElectionsTest(electionsMock: List<Election>) = runBlockingTest {
        val electionCount = electionsMock.size
        scenario.onFragment { fragment ->
            val mockViewState = ElectionsViewState(electionsMock)
            fragment.renderElections(mockViewState)
        }

        onView(withId(R.id.election_master_recycler_view)).check(matches(recyclerViewSizeMatcher(electionCount)))
        onView(withId(R.id.election_master_recycler_view)).check(matches((isDisplayed())))

        var position = 1
        electionsMock.forEach {
            onView(withId(R.id.election_master_recycler_view))
                .perform(RecyclerViewActions.scrollToPosition<ElectionMasterAdapter.ViewHolder>(position))
            onView(withText(it.getTitle())).check(matches(isDisplayed()))
            position++
        }

        onView(withId(R.id.no_elections_bitcoin_unlimited_logo)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_elections_content)).check(matches(not(isDisplayed())))
        onView(withId(R.id.no_elections_title)).check(matches(not(isDisplayed())))
        onView(withId(R.id.open_voter_cash)).check(matches(not(isDisplayed())))
    }

    @Test fun navigateToElectionDetailTest() {
        val election = ElectionFaker.generateOngoingTwoOptionElection("id_mock")
        val electionItemPosition = 0
        scenario.onFragment { fragment ->
            val mockViewState = ElectionsViewState(listOf(election))
            fragment.renderElections(mockViewState)
        }

        onView(withId(R.id.election_master_recycler_view)).check(matches((isDisplayed())))
        onView(withId(R.id.election_master_recycler_view)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ElectionMasterAdapter.ViewHolder>(
                electionItemPosition,
                ViewActions.click()
            )
        )

        val currentDestinationId = navController.currentDestination?.id
        Assertions.assertEquals(currentDestinationId, R.id.nav_election_detail)
        Assertions.assertNotEquals(currentDestinationId, R.id.nav_election_result_fragment)
    }

    @Test fun errorTest() {
        val exceptionTextMock = "Something went wrong in ElectionMasterFragment!"
        val exceptionMock = Exception(exceptionTextMock)
        scenario.onFragment { fragment ->
            val mockViewState = ElectionMasterViewState.ErrorElectionMaster(exceptionMock)
            fragment.render(mockViewState)
        }
        onView(withId(R.id.toolbar_error)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar_error)).check(matches(withText(exceptionTextMock)))
    }

    @Test fun authErrorTest() {
        val exceptionTextMock = "Something went wrong in Auth!"
        val exceptionMock = Exception(exceptionTextMock)
        scenario.onFragment { fragment ->
            val mockViewState = ElectionMasterViewState.Auth(AuthState.AuthError(exceptionMock))
            fragment.render(mockViewState)
        }
        onView(withId(R.id.toolbar_error)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar_error)).check(matches(withText(exceptionTextMock)))
    }

    @Test fun authLoadingTest() {
        scenario.onFragment { fragment ->
            val mockId = "mock_id"
            val authState = AuthState.AuthenticatedAnonymously(mockId)
            val mockViewState = ElectionMasterViewState.Auth(authState)
            fragment.render(mockViewState)
        }
        onView(withId(R.id.authenticating_progress_bar)).check(matches(isDisplayed()))
        onView(withId(R.id.authenticating_text)).check(matches(isDisplayed()))
    }

    @Test fun authenticatedTest() {
        scenario.onFragment { fragment ->
            val mockViewState = ElectionMasterViewState.Auth(AuthState.AuthenticatedKeyPair("user_id_mock"))
            fragment.render(mockViewState)
        }
        onView(withId(R.id.authenticating_progress_bar)).check(matches(not(isDisplayed())))
        onView(withId(R.id.authenticating_text)).check(matches(not(isDisplayed())))
    }

    @Test fun electrumApiDisconnectedTest() {
        val disconnectedMessageMock = "Disconnected!"
        scenario.onFragment { fragment ->
            val mockViewState = ElectrumApiConnectionState.Disconnected(disconnectedMessageMock)
            fragment.renderElectrumApiState(mockViewState)
        }
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withText(disconnectedMessageMock)))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withBackgroundColor(R.color.colorAccent)))
    }

    @Test fun electrumApiConnectingTest() {
        val mockHost = "https:://mock.host/"
        val mockPort = 1337
        val connectingMessageMock = "Connecting to Electrum at: $mockHost:$mockPort"
        scenario.onFragment { fragment ->
            val mockViewState = ElectrumApiConnectionState.Connecting(mockHost, mockPort)
            fragment.renderElectrumApiState(mockViewState)
        }
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withText(connectingMessageMock)))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withBackgroundColor(R.color.colorOrange)))
    }

    @Test fun electionApiConnectedTest() {
        val mockHost = "https:://mock.host/"
        val mockPort = 1337
        val connectingMessageMock = "Connected to Electrum at: $mockHost:$mockPort"
        scenario.onFragment { fragment ->
            val mockViewState = ElectrumApiConnectionState.Connected(mockHost, mockPort)
            fragment.renderElectrumApiState(mockViewState)
        }
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withText(connectingMessageMock)))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withBackgroundColor(R.color.colorPrimary)))
    }

    @Test fun electionApiErrorTest() {
        val messageMock = "Something went wrong in electionApi!"
        val exceptionMock = Exception(messageMock)
        val connectingMessageMock = "ERROR: ${exceptionMock.message}"
        scenario.onFragment { fragment ->
            val mockViewState = ElectrumApiConnectionState.ElectrumApiError(exceptionMock)
            fragment.renderElectrumApiState(mockViewState)
        }
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withText(connectingMessageMock)))
        onView(withId(R.id.toolbar_electrum_api_connection_status)).check(matches(withBackgroundColor(R.color.colorAccent)))
    }
}
