package info.bitcoinunlimited.votepeer.election.fund

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import org.junit.Rule
import org.junit.Test

class BalancesScreenKtTest {

    private val balance = 0.420
    private val balanceRequired = 0.0069
    private val balanceText = "0.42 BCH"
    private val balanceRequiredText = "0.0069 BCH"
    private val balanceLoading = "Contract balance loading..."
    private val voteOptionHuman = "Yes sir!"
    private val contractState = ContractState.Contract(balance, balanceRequired, voteOptionHuman)

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun renderBalanceLoading() {
        composeTestRule.setContent {
            Balances(ContractState.LoadingBalance(balanceLoading, balanceRequired, voteOptionHuman))
        }

        composeTestRule
            .onNodeWithText("Required balance")
            .assertIsDisplayed()

        composeTestRule
            .onNodeWithText("Balance")
            .assertIsDisplayed()

        composeTestRule
            .onNodeWithText(balanceLoading)
            .assertIsDisplayed()
    }

    @Test
    fun renderBalance() {
        composeTestRule.setContent {
            Balances(contractState)
        }

        composeTestRule
            .onNodeWithText("Required balance")
            .assertIsDisplayed()

        composeTestRule
            .onNodeWithText(balanceRequiredText)
            .assertIsDisplayed()

        composeTestRule
            .onNodeWithText("Balance")
            .assertIsDisplayed()

        composeTestRule
            .onNodeWithText(balanceText)
            .assertIsDisplayed()
    }
}