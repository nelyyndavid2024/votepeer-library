package info.bitcoinunlimited.votepeer.election.detail

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.*
import bitcoinunlimited.libbitcoincash.*
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.election.TwoOptionVoteElection
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import io.mockk.*
import kotlinx.coroutines.*
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.not
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

@ExperimentalCoroutinesApi
@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
class ElectionDetailFragmentNotStartedTest {
    companion object {
        init {
            val chain = ChainSelector.BCHMAINNET
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }
    private lateinit var scenario: FragmentScenario<ElectionDetailFragment>
    private lateinit var navController: TestNavHostController
    private val electionMock: TwoOptionVoteElection = ElectionFaker.generateOngoingTwoOptionElection()
    private val currentBlockHeightMock = spyk(electionMock.electionRaw.beginHeight - 100L)
    private val privateKey = ElectionFaker.mockPrivateKey()
    private val privateKeyHex = ToHexStr(privateKey)
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()

    @BeforeEach
    fun setUp() = runBlockingTest {
        mockkStatic(Firebase::class)
        mockkStatic(FirebaseApp::class)
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFunctions::class)
        mockkStatic(ElectrumAPI::class)

        coEvery { ElectrumAPI.getInstance(InjectorUtils.chain).getLatestBlockHeight() } returns currentBlockHeightMock
        val spy = spyk(ElectrumAPI.getInstance(InjectorUtils.chain))
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { ElectrumAPI.getInstance(InjectorUtils.chain) } returns spy
        coEvery { spy.getLatestBlockHeight() } returns currentBlockHeightMock
        every { FirebaseFunctions.getInstance(Constants.region) } returns mockk(relaxed = true)
        navController = TestNavHostController(applicationContext)

        val fragmentArgs = bundleOf(
            "electionTitle" to electionMock.getTitle(),
            "election" to electionMock,
            "currentBlockHeight" to currentBlockHeightMock,
            "privateKeyHex" to privateKeyHex
        )
        // Create a graphical FragmentScenario for the TitleScreen
        scenario = launchFragmentInContainer(fragmentArgs, R.style.AppTheme, Lifecycle.State.STARTED)
        scenario.onFragment { fragment ->
            navController.setGraph(R.navigation.nav_graph_election)
            navController.setCurrentDestination(R.id.nav_election_detail)
            Navigation.setViewNavController(fragment.requireView(), navController)
        }
    }

    @AfterEach
    fun clear() = runBlockingTest {
        scenario.moveToState(Lifecycle.State.DESTROYED)
        clearAllMocks()
    }
}
