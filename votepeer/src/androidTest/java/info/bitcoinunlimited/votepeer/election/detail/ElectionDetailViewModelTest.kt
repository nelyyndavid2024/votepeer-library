package info.bitcoinunlimited.votepeer.election.detail

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.UnsecuredSecret
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDao
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteRoom
import info.bitcoinunlimited.votepeer.vote.Vote
import io.mockk.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionDetailViewModelTest {
    private companion object {
        private val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }

    private lateinit var viewModel: ElectionDetailViewModel
    private lateinit var electionService: ElectionService
    private lateinit var electrumAPI: ElectrumAPI
    private lateinit var voteElectionMock: RingSignatureVoteElection
    private lateinit var currentUserPkhMock: ByteArray
    private lateinit var currentUser: PayDestination
    private lateinit var database: RingSignatureVoteDatabase
    private lateinit var dao: RingSignatureVoteDao
    private lateinit var ringSignatureVoteRepository: RingSignatureVoteRepository
    private lateinit var identityRepository: IdentityRepository
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()
    private val height = 1337L
    private val testDispatcher = TestCoroutineDispatcher()

    @BeforeEach fun setUp() = runBlocking {
        val privateKey = ElectionFaker.mockPrivateKey('L')
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFirestore::class)
        mockkStatic(Firebase::class)
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { Firebase.firestore } returns mockk(relaxed = true)
        voteElectionMock = ElectionFaker.generateOngoingRingSignatureElection("mock_election_id")
        currentUser = Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKey))
        currentUserPkhMock = currentUser.pkh() ?: throw Exception("Missing .pkh")
        identityRepository = IdentityRepository.getInstance(currentUser)
        electionService = spyk(ElectionService.getInstance(chain))
        electrumAPI = spyk(ElectrumAPI.getInstance(chain))
        database = RingSignatureVoteDatabase.getInstance(applicationContext)
        dao = database.ringSignatureVoteDao()
        coEvery { electrumAPI.getLatestBlockHeight() } returns height
    }

    @After
    fun closeDb() = runBlockingTest {
        database.close()
    }

    @AfterEach
    fun clear() = runBlockingTest {
        clearAllMocks()
    }

    @Test fun initTest() {
        ringSignatureVoteRepository = spyk(
            RingSignatureVoteRepository(
                voteElectionMock,
                electionService,
                electrumAPI,
                dao,
                identityRepository
            )
        )
        viewModel = ElectionDetailViewModel(
            testDispatcher,
            voteElectionMock,
            electrumAPI,
            height,
            dao,
            ringSignatureVoteRepository
        )

        verify { viewModel.setHasVoted(false, null) }
    }

    @Test fun initHasVotedTest() = runBlocking {
        val vote = Vote(mockk(relaxed = true), mockk(relaxed = true))
        val ringSignatureVote = RingSignatureVoteRoom(voteElectionMock, vote)
        dao.insert(ringSignatureVote)
        ringSignatureVoteRepository = spyk(
            RingSignatureVoteRepository(
                voteElectionMock,
                electionService,
                electrumAPI,
                dao,
                identityRepository
            )
        )
        coEvery { ringSignatureVoteRepository.getVoteTxId() } returns vote.transaction.voteTxId
        viewModel = ElectionDetailViewModel(
            testDispatcher,
            voteElectionMock,
            electrumAPI,
            height,
            dao,
            ringSignatureVoteRepository
        )

        coVerify { viewModel.setHasVoted(true, vote.transaction.voteTxId) }
    }
}
