package info.bitcoinunlimited.votepeer.election.tally

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.*
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test

class TallyFragmentTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun tallyListRendersCorrectVoteOptionTallyElements() {
        val testData = mapOf("Option 1" to 10, "Option 2" to 20, "Option 3" to 30)

        // Set content to test TallyList Composable
        composeTestRule.setContent {
            TallyList(votes = testData)
        }

        // Check if each VoteOptionTally element displays the correct data
        testData.forEach { (option, vote) ->
            composeTestRule.onNodeWithText(option).assertExists()
            composeTestRule.onNodeWithText(vote.toString()).assertExists()
        }
    }

    @Test
    fun VoteOptionTally_RendersOptionAndVotes() {
        // Arrange
        val option = "Option 1"
        val votes = 5

        // Act
        composeTestRule.setContent {
            VoteOptionTally(option = option, votes = votes)
        }

        // Assert
        composeTestRule.onNodeWithText(option).assertIsDisplayed()
        composeTestRule.onNodeWithText("$votes").assertIsDisplayed()
    }

    @Test
    fun TallyError_displaysErrorMessage_whenErrorIsNotNull() {
        val exception = Exception("Sample error message")

        composeTestRule.setContent {
            TallyError(error = exception)
        }

        composeTestRule.onNodeWithText("Sample error message").assertIsDisplayed()
    }

    @Test
    fun TallyError_doesNotDisplayErrorMessage_whenErrorIsNull() {
        composeTestRule.setContent {
            TallyError(error = null)
        }

        composeTestRule.onNodeWithText("Something went wrong!").assertDoesNotExist()
    }

    @Test
    fun testTallyProgressBar_ShowsLinearProgressIndicatorWhenLoadingIsTrue() {
        // Arrange
        val loading = true

        // Act
        composeTestRule.setContent {
            TallyProgressBar(loading = loading)
        }

        // Assert
        composeTestRule.onNodeWithTag("loading_tally").assertIsDisplayed()
    }

    @Test
    fun testTallyProgressBar_HidesLinearProgressIndicatorWhenLoadingIsFalse() {
        // Arrange
        val loading = false

        // Act
        composeTestRule.setContent {
            TallyProgressBar(loading = loading)
        }

        // Assert
        composeTestRule.onNodeWithTag("loading_tally").assertDoesNotExist()
    }
}
