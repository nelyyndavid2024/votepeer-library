package info.bitcoinunlimited.votepeer.election.tally

import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.VoteRepository
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.election.ElectionService
import info.bitcoinunlimited.votepeer.election.TwoOptionVoteElection
import info.bitcoinunlimited.votepeer.twoOptionVote.TwoOptionVoteRepository
import io.mockk.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class TallyViewModelTest {
    private lateinit var viewModel: TallyViewModel
    private lateinit var electrumAPI: ElectrumAPI
    private lateinit var voteRepository: VoteRepository
    private lateinit var electionService: ElectionService
    private lateinit var currentUser: PayDestination
    private lateinit var electionMock: TwoOptionVoteElection
    private lateinit var identityRepository: IdentityRepository

    private companion object {
        private val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }

    @BeforeEach
    fun setUp() {
        val privateKey = ElectionFaker.mockPrivateKey('L')
        val chain = ChainSelector.BCHMAINNET
        electionMock = ElectionFaker.generateOngoingTwoOptionElection()
        currentUser = Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKey))
        identityRepository = IdentityRepository.getInstance(currentUser)
        electionService = spyk(ElectionService.getInstance(chain))
        electrumAPI = spyk(ElectrumAPI.getInstance(chain))
        voteRepository = TwoOptionVoteRepository(
            chain,
            electionMock,
            electionService,
            electrumAPI,
            identityRepository
        )
        electionMock = spyk(ElectionFaker.generateOngoingTwoOptionElection())
        val voteRepository = TwoOptionVoteRepository(
            chain,
            electionMock,
            electionService,
            electrumAPI,
            identityRepository
        )
        viewModel = spyk(TallyViewModel(identityRepository, voteRepository, electionMock))
    }

    @AfterEach
    fun clear() {
        clearAllMocks()
    }
}
