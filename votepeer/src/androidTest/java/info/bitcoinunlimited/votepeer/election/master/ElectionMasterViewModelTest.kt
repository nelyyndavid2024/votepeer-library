package info.bitcoinunlimited.votepeer.election.master

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.UnsecuredSecret
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVoteContract
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.auth.HttpsProviderFirebase
import info.bitcoinunlimited.votepeer.election.*
import info.bitcoinunlimited.votepeer.election.ElectionServiceTest
import info.bitcoinunlimited.votepeer.election.network.ElectionProviderRest
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDao
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import io.mockk.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
internal class ElectionMasterViewModelTest {
    companion object {
        val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }
    private lateinit var viewModelSpy: ElectionMasterViewModel
    private lateinit var viewModel: ElectionMasterViewModel
    private lateinit var authRepository: AuthRepository
    private lateinit var electionServiceSpy: ElectionService
    private lateinit var electionService: ElectionService
    private lateinit var electrumApi: ElectrumAPI
    private lateinit var contractSpy: TwoOptionVoteContract
    private lateinit var currentUser: PayDestination
    private lateinit var restProviderElectionSpy: ElectionProviderRest
    private lateinit var ringSignatureVoteRepository: RingSignatureVoteRepository
    private lateinit var database: RingSignatureVoteDatabase
    private lateinit var dao: RingSignatureVoteDao
    private lateinit var electionRingSignatureMock: RingSignatureVoteElection
    private lateinit var identityRepository: IdentityRepository
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()
    private val mockUserIdentityAddress = "bitcoincash:qp94x7qc0uh4jex8qqfghwcf37ju2nvp9q5d8rw0s0"
    private val electionMocks = ElectionFaker.generateTwoOptionElections(4)
    private val mockAuthState = AuthState.AuthenticatedKeyPair(mockUserIdentityAddress)
    private val electionMocksSorted = electionMocks.sortedBy { it.endHeight }

    @BeforeEach
    fun setUp() {
        val privateKey = ElectionFaker.mockPrivateKey('L')
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFirestore::class)
        mockkStatic(Firebase::class)
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { Firebase.firestore } returns mockk(relaxed = true)
        every { FirebaseAuth.getInstance().currentUser }
        electionRingSignatureMock = ElectionFaker.generateOngoingRingSignatureElection("mock_id")
        currentUser = Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKey))
        electionServiceSpy = spyk(ElectionService.getInstance(chain))
        val contractElectionId = electionRingSignatureMock.getContractElectionID()
        val contract = TwoOptionVoteContract(
            contractElectionId,
            electionRingSignatureMock.getOptionHash("0"),
            electionRingSignatureMock.getOptionHash("1"),
            currentUser.pkh()!!
        )
        contractSpy = spyk(contract)
        electrumApi = spyk(ElectrumAPI.getInstance(chain))
        identityRepository = IdentityRepository.getInstance(currentUser)
        authRepository = spyk(
            AuthRepository.getInstance(
                identityRepository,
                httpsProviderFirebase = HttpsProviderFirebase,
                initialAuthState = AuthState.AuthenticatedKeyPair(mockUserIdentityAddress)
            )
        )
        restProviderElectionSpy = spyk(ElectionProviderRest)

        electionService = ElectionService.getInstance(ElectionServiceTest.chain)
        database = Room.inMemoryDatabaseBuilder(applicationContext, RingSignatureVoteDatabase::class.java).build()
        dao = database.ringSignatureVoteDao()
        ringSignatureVoteRepository = spyk(
            RingSignatureVoteRepository(
                electionRingSignatureMock,
                electionService,
                electrumApi,
                dao,
                identityRepository
            )
        )
        val applicationMock = mockk<Application>(relaxed = true)
        viewModelSpy = spyk(
            ElectionMasterViewModel(
                electionService,
                authRepository,
                identityRepository,
                electrumApi,
                restProviderElectionSpy,
                chain,
                applicationMock
            )
        )
        viewModel = ElectionMasterViewModel(
            electionService,
            authRepository,
            identityRepository,
            electrumApi,
            restProviderElectionSpy,
            chain,
            applicationMock
        )
        electionService = ElectionService.getInstance(chain)
    }

    @AfterEach
    fun clear() {
        clearAllMocks()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun observeElectrumConnectionStatusTest() {
        val messageMock = "Disconnected"
        val electionApiConnectionStateMock = ElectrumApiConnectionState.Disconnected(messageMock)
        every { electrumApi.connectionState.value } returns electionApiConnectionStateMock
        viewModelSpy.observeElectrumApiStatus()
    }

    @Test
    fun fetchElectionsNetworkIfAuthenticatedTest() = runBlockingTest {
        every { authRepository.authState.value } returns mockAuthState
        coEvery { restProviderElectionSpy.getElections() } returns electionMocks
        val elections = viewModelSpy.fetchElectionsNetworkIfAuthenticated(mockAuthState)
        coVerify { restProviderElectionSpy.getElections() }
        assertEquals(electionMocksSorted, elections)
    }
}
