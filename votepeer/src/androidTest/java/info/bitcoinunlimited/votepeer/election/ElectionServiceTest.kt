package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.*
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVoteContract
import info.bitcoinunlimited.votepeer.vote.Vote
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteTransaction
import io.mockk.clearAllMocks
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
internal class ElectionServiceTest {
    private lateinit var electionService: ElectionService
    private lateinit var twoOptionVoteElectionMock: TwoOptionVoteElection

    companion object {
        val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }

    private val optionA = "yes"
    private val optionB = "no"
    private val salt = "unittest"

    @BeforeEach fun init() {
        twoOptionVoteElectionMock = ElectionFaker.generateOngoingTwoOptionElection()
        electionService = ElectionService.getInstance(chain)
    }

    @AfterEach
    fun clear() {
        clearAllMocks()
    }

    @Test
    fun parseVoteScriptEmpty() {
        val voteTxMock = BCHtransaction(chain)
        val privateKey: ByteArray = ElectionFaker.mockPrivateKey()
        val inputAmount = (
            -1 + TwoOptionVoteContract.MIN_CONTRACT_INPUT +
                TwoOptionVoteContract.MIN_CHANGE_OUTPUT + TwoOptionVoteContract.FUND_FEE
            )
        val inputOne = ElectionFaker.bchInput(chain, privateKey, inputAmount)
        voteTxMock.inputs.add(inputOne)
        val voteTransactionMock = VoteTransaction.TwoOption(voteTxMock)
        val exception = org.junit.jupiter.api.assertThrows<IllegalStateException> {
            electionService.parseVote(voteTransactionMock, twoOptionVoteElectionMock)
        }
        assertEquals(exception.message, "tx.inputs[0].script is an empty array")
    }

    @Test
    fun parseVoteInputEmptyInputArray() {
        val tx = BCHtransaction(chain)
        val inputEmptyVoteTransactionMock = VoteTransaction.TwoOption(tx)
        val exception = org.junit.jupiter.api.assertThrows<IndexOutOfBoundsException> {
            electionService.parseVote(inputEmptyVoteTransactionMock, twoOptionVoteElectionMock)
        }
        assertEquals(exception.message, "tx.inputs.size == 0, should be one or more")
    }

    @Test
    fun parseVoteMultipleTxInputs() {
        val tx = BCHtransaction(chain)
        val privateKey: ByteArray = ElectionFaker.mockPrivateKey()
        val inputAmount = (
            -1 + TwoOptionVoteContract.MIN_CONTRACT_INPUT +
                TwoOptionVoteContract.MIN_CHANGE_OUTPUT + TwoOptionVoteContract.FUND_FEE
            )
        val inputOne = ElectionFaker.bchInput(chain, privateKey, inputAmount)
        tx.inputs.add(inputOne)
        tx.inputs.add(inputOne)
        val transactionMock = VoteTransaction.TwoOption(tx)
        val exception = org.junit.jupiter.api.assertThrows<IndexOutOfBoundsException> {
            electionService.parseVote(transactionMock, twoOptionVoteElectionMock)
        }
        assertEquals(exception.message, "Multiple inputs NYI")
    }

    @Test
    fun getAbstainVoteSentence() {
        val abstainVoteTx = ElectionFaker.getAbstainVoteTransactions(optionA, optionB, salt)
        val abstainVoteTransaction = VoteTransaction.TwoOption(abstainVoteTx)
        val election = ElectionFaker.createTwoOptionElectionWithAbstainVote()
        val voteOptionHash = electionService.parseVote(abstainVoteTransaction, election)
        val voteSentence = electionService.parseVoteSentence(election, voteOptionHash)
        assertEquals(voteSentence, "✅ You voted abstain")
    }

    @Test
    fun getOptionAVoteSentence() {
        val twoOptionElectionMock = ElectionFaker.generateOngoingTwoOptionElection()
        val selectedOption = optionA
        val voteTx = ElectionFaker.getVoteTxTwoOptionVote(selectedOption, salt, optionA, optionB)
        val election = twoOptionElectionMock.copy(
            electionRaw = twoOptionElectionMock.electionRaw.copy(
                options = arrayOf("yes", "no"),
                salt = salt
            )
        )

        val voteOptionHash = electionService.parseVote(voteTx, election)
        val voteSentence = electionService.parseVoteSentence(election, voteOptionHash)
        assertEquals(voteSentence, "✅ You voted ${election.options[0]}")
    }

    @Test
    fun getOptionBVoteSentence() {
        val selectedOption = optionB
        val voteTx = ElectionFaker.getVoteTxTwoOptionVote(selectedOption, salt, optionA, optionB)
        val election = twoOptionVoteElectionMock.copy(
            electionRaw = twoOptionVoteElectionMock.electionRaw.copy(
                options = arrayOf(optionA, optionB),
                salt = salt
            )
        )

        val voteOptionHash = electionService.parseVote(voteTx, election)
        val voteSentence = electionService.parseVoteSentence(election, voteOptionHash)
        assertEquals(voteSentence, "✅ You voted ${election.options[1]}")
    }

    @Test
    fun getVoteOptionA() {
        val selectedOption = optionA
        val election = twoOptionVoteElectionMock.copy(
            electionRaw = twoOptionVoteElectionMock.electionRaw.copy(
                options = arrayOf(optionA, optionB),
                salt = salt
            )
        )
        val voteTx = ElectionFaker.getVoteTxTwoOptionVote(selectedOption, salt, optionA, optionB)
        val vote = electionService.getVote(election, voteTx)
        assertEquals(vote, Vote(VoteOption("0", vote.option.selectedTime), voteTx))
    }

    @Test
    fun getVoteOptionB() {
        val selectedOption = optionB
        val election = twoOptionVoteElectionMock.copy(
            electionRaw = twoOptionVoteElectionMock.electionRaw.copy(
                options = arrayOf(optionA, optionB),
                salt = salt
            )
        )

        val voteTx = ElectionFaker.getVoteTxTwoOptionVote(selectedOption, salt, optionA, optionB)
        val vote = electionService.getVote(election, voteTx)
        assertEquals(vote, Vote(VoteOption("1", vote.option.selectedTime), voteTx))
    }

    @Test
    fun getContractElectionIdTest() {
        val election = twoOptionVoteElectionMock.copy(
            electionRaw = twoOptionVoteElectionMock.electionRaw.copy(
                options = arrayOf(optionA, optionB),
            )
        )
        val payDestination: PayDestination = Pay2PubKeyHashDestination(
            chain, UnsecuredSecret(ElectionFaker.mockPrivateKey('B'))
        )
        val contractElectionId = election.getContractElectionID()
        val contract = TwoOptionVoteContract(
            contractElectionId,
            election.getOptionHash("0"),
            election.getOptionHash("1"),
            payDestination.pkh()!!
        )
        MatcherAssert.assertThat(contract, CoreMatchers.instanceOf<Any>(TwoOptionVoteContract::class.java))
        MatcherAssert.assertThat(contract.optA, CoreMatchers.instanceOf<Any>(ByteArray::class.java))
        MatcherAssert.assertThat(contract.optB, CoreMatchers.instanceOf<Any>(ByteArray::class.java))
    }

    @Test
    fun parseVoteTwoOptionCrashNoScript() {
        val privateKey: ByteArray = ElectionFaker.mockPrivateKey()
        val inputAmount = (TwoOptionVoteContract.MIN_CONTRACT_INPUT + TwoOptionVoteContract.FUND_FEE)
        val changeAddress: PayDestination = Pay2PubKeyHashDestination(
            chain, UnsecuredSecret(ElectionFaker.mockPrivateKey('B'))
        )
        val contract = ElectionFaker.mockTwoOptionVoteContracts(
            twoOptionVoteElectionMock.options[0],
            twoOptionVoteElectionMock.options[1],
            twoOptionVoteElectionMock.salt.toByteArray()
        )[0]
        val fundCoin = ElectionFaker.bchInput(chain, privateKey, inputAmount)
        // TODO: Use valid transaction
        val fundTx = contract.fundContract(chain, fundCoin, changeAddress)
        val exception = org.junit.jupiter.api.assertThrows<IllegalStateException> {
            electionService.parseVote(VoteTransaction.TwoOption(fundTx), twoOptionVoteElectionMock)
        }

        assertEquals(exception.message, "Expected push 40")
    }
}