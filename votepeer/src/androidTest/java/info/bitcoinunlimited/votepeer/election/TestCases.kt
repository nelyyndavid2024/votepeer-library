package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.BCHtransaction
import bitcoinunlimited.libbitcoincash.ChainSelector
import java.util.* // ktlint-disable no-wildcard-imports
import java.util.stream.Stream

object TestCases {
    data class GetCurrentUserBallot(
        val bchTransactionMock: BCHtransaction?
    )

    @JvmStatic
    fun getCurrentUserBallotTestCases(): Stream<GetCurrentUserBallot> =
        Arrays.stream(
            arrayOf(
                GetCurrentUserBallot(BCHtransaction(ChainSelector.BCHMAINNET)),
                GetCurrentUserBallot(null)
            )
        )
}
